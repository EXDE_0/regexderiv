# Requirements:
openjdk 17 (Or other equivalent jdk)

# Compile instructions:
git clone https://gitlab.com/EXDE_0/regexderiv.git
javac -d distro *.java ; java -cp distro/ gitRegexDeriv.Main

cd gitRegexDeriv

javac -d distro src/main/\*.java src/testing/\*.java src/DFA/\*.java src/DFA/similarityOp/\*.java

## Testing instructions:
javac -d distro src/main/\*.java src/testing/\*.java src/DFA/\*.java src/DFA/similarityOps/\*.java ; java -ea -cp distro/ gitRegexDeriv.src.main.Main

java -ea -cp distro/ gitRegexDeriv.src.main.Main

## Create .jar
jar cvfm regexDeriv.jar Manifest.txt distro/gitRegexDeriv/src/main/Main.class distro/*
