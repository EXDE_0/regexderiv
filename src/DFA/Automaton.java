package gitRegexDeriv.src.DFA;

import java.util.ArrayList;
import java.util.List;

public class Automaton{
    List<Node> nodes;

    public Automaton() {
        this.nodes = new ArrayList<>();
    }

    @Override
    public String toString() {
        String retString = "";

        for (Node n : nodes){
            retString += n.toString() + "\n";
        }

        return retString;
    }

    public List<Node> getNodes() {
        return nodes;
    }

}