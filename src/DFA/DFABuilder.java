package gitRegexDeriv.src.DFA;

import gitRegexDeriv.src.DFA.similarityOps.AlphabeticalOrdering;
import gitRegexDeriv.src.DFA.similarityOps.DupeRemove;
import gitRegexDeriv.src.DFA.similarityOps.EpsylonRemove;
import gitRegexDeriv.src.DFA.similarityOps.SingleOperandRemove;
import gitRegexDeriv.src.DFA.similarityOps.bracketOrdering;
import gitRegexDeriv.src.main.Commons;
import gitRegexDeriv.src.main.DerivationOps;

import java.util.List;

public class DFABuilder {
    DerivationOps derOps = new DerivationOps();

    public Automaton buildDFA( String regex, List<String> alphabet ) /*throws Exception*/ {
        var retDFA = new Automaton();
        var processedRegex = AlphabeticalOrdering.sort(bracketOrdering.moveBrackets(DupeRemove.removeDublicates(EpsylonRemove.rewrite(SingleOperandRemove.rewrite(regex)))));

        // add starting state
        boolean accpetingState = derOps.nullableFull(processedRegex, alphabet).equals("∅") ? false : true;
        retDFA.nodes.add( new Node(Commons.getNodeID(), true, accpetingState, processedRegex) );

        //iterate over existing states (starts with only start state but gets filled up)
        for ( int i = 0; i < retDFA.nodes.size(); i++ ) {
            Node node = retDFA.nodes.get(i);

            //iterate over letters of the alphabet
            for (String letter : alphabet) {
                String derivedRegex = deriveStateForLetter(node, letter, alphabet);
                String derivedProcessedRegex = AlphabeticalOrdering.sort(bracketOrdering.moveBrackets(DupeRemove.removeDublicates(EpsylonRemove.rewrite(SingleOperandRemove.rewrite(derivedRegex)))));

                boolean equivalentFound = false;

                //check for equivalence
                for (int j = 0; j < retDFA.nodes.size(); j++) {
                    Node nodeCheck = retDFA.nodes.get(j);

                    if ( derivedProcessedRegex.equals( nodeCheck.getRepresentedRegex() ) ) {
                        node.getOutGoingTransitions().put(letter, nodeCheck);
                        equivalentFound = true;
                    }
                }

                //didn't find equivalent node
                if ( !equivalentFound ) {
                    Node newNode = new Node();
                    newNode.setID(Commons.getNodeID());
                    newNode.setStartNode(false);
                    newNode.setRepresentedRegex(derivedProcessedRegex);

                    if ( derOps.nullableFull(derivedProcessedRegex, alphabet).equals("∅") ) {
                        newNode.setAccpetingNode(false);
                    } else if ( derOps.nullableFull(derivedProcessedRegex, alphabet).equals("ε") ) {
                        newNode.setAccpetingNode(true);
                    } else {
                        System.err.println("DX");
                        //throw new Exception("Error in DFA nullable check");
                    }

                    node.getOutGoingTransitions().put(letter, newNode);
                    retDFA.nodes.add(newNode);
                }

            }
        }

        return retDFA;
    }

    private String deriveStateForLetter(Node node, String letter, List<String> alphabet) {
        return derOps.derive(letter, node.getRepresentedRegex(), alphabet );
    }


}
