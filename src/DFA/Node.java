package gitRegexDeriv.src.DFA;

import java.util.HashMap;

public class Node {
    private long ID;
    private HashMap<String, Node> outGoingTransitions; //letter, node ID
    private boolean startNode;
    private boolean accpetingNode;
    private String representedRegex;

    public Node() {
        this.ID = -1;
        this.outGoingTransitions = new HashMap<String, Node>();
        this.representedRegex = "";
    }

    public Node(long id, HashMap<String, Node> trans, Boolean stNode, Boolean acptNode, String reprRegex) {
        this.ID = id;
        this.outGoingTransitions = trans;
        this.startNode = stNode;
        this.accpetingNode = acptNode;
        this.representedRegex = reprRegex;
    }

    public Node(HashMap<String, Node> trans, Boolean stNode, Boolean acptNode, String reprRegex) {
        this.outGoingTransitions = trans;
        this.startNode = stNode;
        this.accpetingNode = acptNode;
        this.representedRegex = reprRegex;
    }

    public Node(long id, Boolean stNode, Boolean acptNode, String reprRegex) {
        this.ID = id;
        this.outGoingTransitions = new HashMap<String, Node>();
        this.startNode = stNode;
        this.accpetingNode = acptNode;
        this.representedRegex = reprRegex;
    }

    @Override
    public String toString() {
        if( outGoingTransitions.isEmpty() ){
            return (this.ID != -1 ? "ID: " + this.ID : "") + "\n" +
                "Startnode: " + this.startNode + "\n" +
                "AcceptingNode: " + this.accpetingNode + "\n" +
                "Regex: " + this.representedRegex;
        } else {
            String oGTransitionsText = "";

            int counter = 0;
            for (String i : this.outGoingTransitions.keySet()) {
                oGTransitionsText += i + ":" + this.outGoingTransitions.get(i).ID;
                if ( counter < this.outGoingTransitions.size() ) oGTransitionsText += "\n";
                ++counter;
              }

              return (this.ID != -1 ? "ID: " + this.ID : "") + "\n" +
                "Startnode: " + this.startNode + "\n" +
                "AcceptingNode: " + this.accpetingNode + "\n" +
                "Regex: " + this.representedRegex + "\n" +
                "Transitions: \n" + oGTransitionsText;
        }
    }

    public long getID() {
        return ID;
    }

    public void setID(long iD) {
        ID = iD;
    }

    public HashMap<String, Node> getOutGoingTransitions() {
        return outGoingTransitions;
    }

    public void setOutGoingTransitions(HashMap<String, Node> outGoingTransitions) {
        this.outGoingTransitions = outGoingTransitions;
    }

    public boolean getStartNode() {
        return startNode;
    }

    public void setStartNode(boolean startNode) {
        this.startNode = startNode;
    }

    public boolean getAccpetingNode() {
        return accpetingNode;
    }

    public void setAccpetingNode(boolean accpetingNode) {
        this.accpetingNode = accpetingNode;
    }

    public String getRepresentedRegex() {
        return representedRegex;
    }

    public void setRepresentedRegex(String representedRegex) {
        this.representedRegex = representedRegex;
    }
}
