package gitRegexDeriv.src.DFA;

public class runDFA {
    public static String execute(String input, Automaton automaton) {
        Node activeNode = automaton.nodes.get(0); // get the starting node

        for(int i = 0; i < input.length(); i++) {
            try{
                //get the node that is connected to
                activeNode = activeNode.getOutGoingTransitions().get(String.valueOf(input.charAt(i)));
            } catch (NullPointerException e) {
                return "No match";
            } catch (Exception e) {
                System.err.println("No such input!");
                //e.printStackTrace();
                return "";
            }
        }

        if (null != activeNode){
            return activeNode.getAccpetingNode() ? "Match" : "No match";
        } else {
            return "No match";
        }

    }

    private runDFA(){};
}
