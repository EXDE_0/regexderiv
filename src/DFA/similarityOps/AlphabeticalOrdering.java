package gitRegexDeriv.src.DFA.similarityOps;

import java.util.ArrayList;

import gitRegexDeriv.src.main.Atomic;
import gitRegexDeriv.src.main.Commons;
import gitRegexDeriv.src.main.Atomic.*;

public final class AlphabeticalOrdering {

    private static final int OFFSET = 2; //the offset from our calculated position to our actual character

    // "+(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r))))" og. regex

    // "+(+(a, b), +(c, z)),                                     r side
    // +(+(d, x), °(+(g, j), +(o, r)))"                          s side

    // a, b, c, z, d, x, °(+(g, j), +(o, r))                     flatten
    // a, b, c, d, °(+(g, j), +(o, r)), x, z                     sorted

    // "+(+(+(a, b), +(c, d)), +(°(+(g, j), +(o, r))), +(x, z))  rebuilt
    // "+(+(+(a, b), +(c, d)), +(+(°(+(g, j), +(o, r))), x), z)  rebuilt alt
    public static String sort( String regex ) {
        ArrayList<ArrayList<Atomic>> indexes = findatomicsExtendedAT( regex, String.valueOf(regex.charAt(0)), 0 );
        String retRegex = rebuildRegex( indexes, regex );

        return retRegex;
    }

    // finds the indexes for symbols and subregexes
    // where changes can occur

    //AT = 'Atomic Type' (uses the atomic type)
    private static ArrayList<Atomic> findatomicsAT( String regex, String mainOp, String parentOp, int currentIndex, int parentID ) {

        if(mainOp.isEmpty()){
            return new ArrayList<Atomic>() {{
                new Atomic( Commons.getAtomicID(), parentID, Atomic.AtomicType.CHARACTER, 0, 1, "");
            }};
        }

        if ( !mainOp.equals("+") && Commons.ops.contains(mainOp) ) {
            return new ArrayList<Atomic>() {{
                if( parentOp.isEmpty() ) {
                    add(
                        new Atomic( Commons.getAtomicID(), parentID, Atomic.AtomicType.SUBREGEX, currentIndex, regex.length(), mainOp )
                    );
                }
                else {
                    add(
                        new Atomic( Commons.getAtomicID(), parentID, Atomic.AtomicType.SUBREGEX, currentIndex, regex.length(), parentOp )
                    );
                }
            }};
        }

        ArrayList<Atomic> retList = new ArrayList<Atomic>();

        int borderIndex = ( regex.charAt(0) != '*' || regex.charAt(0) != Commons.NEGATION.charAt(0) ) //not a single arg op
            && Commons.ops.contains( String.valueOf( regex.charAt(0) ) ) ? //is it an operand?
                Commons.findBorder(regex) : regex.length();
        String r = regex.length() > 1 ? Commons.getR(regex, borderIndex) : regex;
        String s = borderIndex != regex.length() ? Commons.getS(regex, borderIndex) : "";

        if( r.charAt(0) == mainOp.charAt(0) ) //r's mainop is +
            retList.addAll( findatomicsAT( r, String.valueOf( r.charAt(0)), mainOp, currentIndex + OFFSET, parentID ) );
        else if ( Commons.ops.contains( String.valueOf(r.charAt(0)) ) ) {              //r is a regex that we cannot atomize (r's mainop isn't +)
            int added_nu = currentIndex + OFFSET;
            if( parentOp.isEmpty() || mainOp.equals("+") ) retList.add( new Atomic( Commons.getAtomicID(), parentID, Atomic.AtomicType.SUBREGEX, added_nu, r.length(), mainOp ) );
            else  retList.add( new Atomic( Commons.getAtomicID(), parentID, Atomic.AtomicType.SUBREGEX, added_nu, r.length(), parentOp ) );
        } else if ( !Commons.ops.contains(r) ) {               //r is a symbol in the alphabet
            int added_nu = currentIndex + OFFSET;
            retList.add( new Atomic( Commons.getAtomicID(), parentID, Atomic.AtomicType.CHARACTER, added_nu, 1, mainOp ) );
        }

        if(!s.equals("")){
            if( s.charAt(0) == mainOp.charAt(0) ) { // s's mainop is +
                retList.addAll( findatomicsAT( s, String.valueOf(s.charAt(0)), mainOp, currentIndex + borderIndex + OFFSET, parentID ) );
            } else if ( Commons.ops.contains( String.valueOf(s.charAt(0)) ) ) {                //s is a regex that we cannot atomize
                int added_nu = currentIndex + borderIndex + OFFSET;
                if ( parentOp.isEmpty() || mainOp.equals("+") ) retList.add( new Atomic( Commons.getAtomicID(), parentID, Atomic.AtomicType.SUBREGEX, added_nu, s.length(), mainOp ) );
                else retList.add( new Atomic( Commons.getAtomicID(), parentID, Atomic.AtomicType.SUBREGEX, added_nu, s.length(), parentOp ) );
            } else if ( !Commons.ops.contains(s) ) {               //s is a symbol in the alphabet
                int added_nu = currentIndex + borderIndex + OFFSET;
                retList.add(new Atomic( Commons.getAtomicID(), parentID, Atomic.AtomicType.CHARACTER, added_nu, 1, mainOp ));
            }
        }

        return retList;
    }

    /*
      °(°(+(b, c), +(+(a, d), +(f, a))), °(a, b))
      [ [6, 9], [17, 20, 26, 29] ]

      °(°(+(b, c), +(+(a, d), +(f, °(a, +(k, g))))), °(a, b))
      [ [6, 9], [17, 20, 26, 29], [36, 39] ]
    */

    public static ArrayList<ArrayList<Atomic>> findatomicsExtendedAT( String regex, String mainOp, int currentIndex ) {

        //if the regex is just one chararcter
        if(regex.length() == 1){
            ArrayList<ArrayList<Atomic>> retList = new ArrayList<>();
            retList.add( new ArrayList<>() );
            retList.get(0).add( new Atomic(AtomicType.CHARACTER, 0, 1, "") );
            return retList;
        }

        ArrayList<ArrayList<Atomic>> retList = new ArrayList<ArrayList<Atomic>>();
        retList.add( findatomicsAT(regex, mainOp, "", currentIndex, -1) );

        for( int i = 0; i < retList.size(); i++ ) {
            for( int j = 0; j < retList.get(i).size(); j++ ) {
                int currPlace = retList.get(i).get(j).getIndex(); //number in regex
                //currmainOp is either *, negation or a singular letter that is to be found (in which case it is an empty string)
                //or the actual main op
                String currMainOp = regex.substring(currPlace, currPlace+1);

                if ( Commons.ops.contains( currMainOp ) && !currMainOp.equals("") ) {
                    String innerSubRegex = regex.substring( currPlace,
                        1+currPlace+Commons.findEndOfRegex( regex.substring(currPlace) ) );
                    ArrayList<Atomic> innerAtomics = new ArrayList<Atomic>();
                    ArrayList<Atomic> innerAtomicsS = new ArrayList<Atomic>();

                    int innerBorderIndex = innerSubRegex.charAt(0) != '*' || innerSubRegex.charAt(0) != Commons.NEGATION.charAt(0) ?
                        Commons.ops.contains( String.valueOf( innerSubRegex.charAt(0) ) ) ?
                        Commons.findBorder(innerSubRegex) : -1 : innerSubRegex.length();
                    String innerR = "";
                    String innerS = "";

                    if( innerBorderIndex != -1 ){

                        innerR = Commons.getR(innerSubRegex, innerBorderIndex);
                        String currentMainOpR = String.valueOf( innerR.charAt(0) );

                        if( Commons.ops.contains( String.valueOf(innerR.charAt(0)) ) ){
                            innerAtomics.addAll( findatomicsAT(innerR, currentMainOpR, currMainOp, currPlace+2, retList.get(i).get(j).getID()) );
                        }

                        if( !currMainOp.equals("*") && !currMainOp.equals(Commons.NEGATION) ) { //is there an "s" side?
                            innerS = Commons.getS(innerSubRegex, innerBorderIndex);
                            String currentMainOpS = String.valueOf( innerS.charAt(0) );

                            //System.out.println("main op is:" + currMainOp);
                            //added innerAtomicsS - do testing on this
                            if( Commons.ops.contains( String.valueOf(innerS.charAt(0)) ) ){
                                innerAtomicsS.addAll( findatomicsAT(innerS, currentMainOpS, currMainOp, currPlace+2+innerBorderIndex, retList.get(i).get(j).getID()) );
                            }
                        }

                        if ( !innerAtomics.isEmpty() ) retList.add(innerAtomics);
                        if ( !innerAtomicsS.isEmpty() ) retList.add(innerAtomicsS);
                    }
                }
                else {
                    continue;
                }
            }
        }

        return retList;
    }


    // Takes a regex and returns a list of it's atomic values
    // The index inside the string is it's position inside the
    // regex
    public static ArrayList<String> flatten(ArrayList<Atomic> atomics, String regex) {
        //TODO
        ArrayList<String> retList = new ArrayList<String>();

        for ( Atomic atomic : atomics ) {
            retList.add( regex.substring( atomic.getIndex(), atomic.getIndex()+atomic.getSize() ) );
        }
        return retList;
    }

    //Sorts a single layer of atomics
    public static ArrayList<Atomic> sortRegex(ArrayList<Atomic> elements, String regex) { //TODO this should be private
        Atomic tmp;
        boolean swapped;
        ArrayList<Atomic> sortedElements = new ArrayList<>();

        for( var element : elements ) {
            sortedElements.add(element);
        }

        for( int i = 0; i < sortedElements.size() - 1; i++ ) {
            swapped = false;

            for( int j = 0; j < sortedElements.size() - i - 1; j++ ) {
                Atomic part = sortedElements.get(j);
                Atomic nextPart = sortedElements.get(j+1);
                String partText = "";
                String nextPartText = "";

                //TODO INTO A FUCNTION
                if ( part.getType() == AtomicType.CHARACTER )
                    partText = regex.substring(part.getIndex(), part.getIndex()+part.getSize());
                else{
                    partText = getCharactersFromSubregex(part, regex);
                }

                if ( nextPart.getType() == AtomicType.CHARACTER )
                    nextPartText = regex.substring(nextPart.getIndex(), nextPart.getIndex()+nextPart.getSize());
                else
                    nextPartText = getCharactersFromSubregex(nextPart, regex);

                if( partText.compareTo(nextPartText) > 0 ) {
                    tmp = sortedElements.get(j);
                    sortedElements.set(j, sortedElements.get(j + 1));
                    sortedElements.set(j + 1, tmp);
                    swapped = true;
                }
            }

            if ( !swapped ){
                break;
            }
        }

        return sortedElements;
    }

    private static String getCharactersFromSubregex(Atomic subregex, String regex){
        String retVal = "";
        String innerRegex = regex.substring(subregex.getIndex(), subregex.getSize() + subregex.getIndex());

        if(subregex.getType() == AtomicType.CHARACTER) {
            retVal = retVal.concat(innerRegex);
        } else {
            return getCharactersFromSubregexCarryMainOp( subregex, regex, regex.substring( subregex.getIndex(), subregex.getIndex() + 1 ), retVal );
        }
        return retVal;
    }

    private static String getCharactersFromSubregexCarryMainOp(Atomic subregex, String regex, String prevMainOp, String retval){
        String retVal = "";
        String innerRegex = regex.substring(subregex.getIndex(), subregex.getSize() + subregex.getIndex());

        if(subregex.getType() == AtomicType.CHARACTER) {
            retVal = retVal.concat(innerRegex);
        } else {
            String mainop = regex.substring(subregex.getIndex(), subregex.getIndex() + 1);

            if( mainop.equals("*") || mainop.equals("¬") ) {
                int innerBorderIndex = subregex.getSize();

                String innerR = Commons.getR(innerRegex, innerBorderIndex);
                AtomicType atomictype = Commons.ops.contains( String.valueOf(innerR.charAt(0)) ) ?
                    AtomicType.SUBREGEX : AtomicType.CHARACTER;
                var index = subregex.getIndex() + 2;
                var size = subregex.getSize() - 3;//Commons.findEndOfRegex(innerR) - subregex.getIndex();
                var parentOp = prevMainOp;
                var AtomoicOfR = new Atomic(atomictype, index, size, parentOp);

                return getCharactersFromSubregexCarryMainOp(AtomoicOfR, regex, mainop, retVal);
            } else {
                int innerBorderIndex = Commons.findBorder(innerRegex);
                String innerR = Commons.getR(innerRegex, innerBorderIndex);
                String innerS = Commons.getS(innerRegex, innerBorderIndex);

                AtomicType atomictypeR = Commons.ops.contains( String.valueOf(innerR.charAt(0)) ) ?
                    AtomicType.SUBREGEX : AtomicType.CHARACTER;
                var indexR = subregex.getIndex()+2;
                var sizeR = atomictypeR == AtomicType.SUBREGEX ? Commons.findEndOfRegex(innerR)+1 : 1;
                var parentOpR = mainop;
                var AtomoicOfR = new Atomic(atomictypeR, indexR, sizeR, parentOpR);

                AtomicType atomictypeS = Commons.ops.contains( String.valueOf(innerS.charAt(0)) ) ?
                    AtomicType.SUBREGEX : AtomicType.CHARACTER;
                var indexS = regex.indexOf(innerS, subregex.getIndex());
                var sizeS = atomictypeS == AtomicType.SUBREGEX ? Commons.findEndOfRegex(innerS)+1 : 1;
                var parentOpS = mainop;
                var AtomoicOfS = new Atomic(atomictypeS, indexS, sizeS, parentOpS);

                return getCharactersFromSubregexCarryMainOp(AtomoicOfR, regex, mainop, retVal) +
                    getCharactersFromSubregexCarryMainOp(AtomoicOfS, regex, mainop, retVal);
            }
        }

        return retVal;
    }

    public static String rebuildRegex(ArrayList<ArrayList<Atomic>> atomics, String regex) {
        String retRegex = regex;

        //empty indexes or regex
        if( atomics.size() == 0 || regex.equals("") ){
            return "";
        }
        var sortedAtomics = sortRegexAtomics(atomics, regex);

        //System.out.println(flatten(sortedAtomics.get(0), regex));

        //clear regex
        //replace all characters that can be sorted with _ es
        //replace all subregexes that have child elements (by checking parent ids) with _ es
        String tmpSortedRegex = regex;
        for(int i = atomics.size(); i > 0; i-- ) {
            int counter = i - 1;

            if( atomics.get(counter).size() == 1 ){
                continue;
            }

            for ( int j = atomics.get(counter).size(); j > 0 ; j-- ){
                int counterJ = j - 1;

                int from = atomics.get(counter).get(counterJ).getIndex();
                int to = from + atomics.get(counter).get(counterJ).getSize();

                //retRegex = retRegex.replace( retRegex.substring(from, to), "_" ); //TODO make this work with replaceFirst
                retRegex = retRegex.substring( 0, from ) + "_" + retRegex.substring(to);
                //System.out.println("emptying:" + retRegex); //TODO
            }

            //after 1st round of replacing to _ es, start filling out the regex with the right atomics
            //break;

            for ( int j = 0; j < sortedAtomics.get(counter).size(); j++ ){
                int from = sortedAtomics.get(counter).get(j).getIndex();
                int to = from + sortedAtomics.get(counter).get(j).getSize();

                retRegex = retRegex.replaceFirst( "_", tmpSortedRegex.substring(from, to) );
                //System.out.println("filling: " + retRegex); //TODO
            }
            tmpSortedRegex = retRegex;
        }

        return retRegex;
    }

    //Builds the whole sorted regex backwards
    private static ArrayList<ArrayList<Atomic>> sortRegexAtomics(ArrayList<ArrayList<Atomic>> atomics, String regex){
        ArrayList<ArrayList<Atomic>> sortedAtomics = new ArrayList<>();

        //get sorted atomics
        for( int i = atomics.size(); i > 0; i-- ){
            ArrayList<ArrayList<Atomic>> tmpAtomics = new ArrayList<>();

            for( int j = 0; j < i; j++ ){
                tmpAtomics.add(atomics.get(j));
            }
            for ( int j = i; j < atomics.size(); j++ ){
                tmpAtomics.add(atomics.get(j));
            }

            sortedAtomics.add(0, sortRegex(tmpAtomics.get(i-1), regex));
        }

        return sortedAtomics;
    }

    private static String regexToString(String regex){
        String regexLetters = regex;

        for(char letter : Commons.specialCharacters.toCharArray()) {
            regexLetters = regexLetters.replace(String.valueOf(letter), "");
        }

        return regexLetters;
    }

    private AlphabeticalOrdering() {}
}