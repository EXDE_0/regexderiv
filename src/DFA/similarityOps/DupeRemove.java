package gitRegexDeriv.src.DFA.similarityOps;

import gitRegexDeriv.src.main.Commons;

public final class DupeRemove {

    public static String removeDublicates(String regex) {
        String retRegex = regex;

        retRegex = removeDublicatesTrackIndex( retRegex, 0, "+" ); //rename this
        retRegex = removeDublicatesTrackIndex( retRegex, 0, "&" );

        return retRegex;
    }

    //r + r ~ r
    //exteded ver: r + (r + s) ~ r + s
    //r + (s + r) ~ r (due to alphabetic sorting this might not be necessary)
    //(r + s) + r
    //(s + r) + r
    private static String removeDublicatesTrackIndex(String regex, int index, String op) {
        int orOpIndex = regex.indexOf(op, index);

        //no more or operands
        if(orOpIndex == -1) return regex;

        int endOfInnerRegexIndex = Commons.findEndOfRegex( regex.substring(orOpIndex) );
        String remainingRegex = regex.substring(orOpIndex, orOpIndex+endOfInnerRegexIndex+1);
        int borderIndex = Commons.findBorder( remainingRegex );

        String R = Commons.getR(remainingRegex, borderIndex);
        String S = Commons.getS(remainingRegex, borderIndex);

        String retRegex = regex;

        // r + r
        if( R.equals(S) ){
            retRegex = regex.substring(0, orOpIndex) + R + regex.substring(orOpIndex + endOfInnerRegexIndex+1); //+1 because we need to get rid of the residual closing bracket
        }

        //get RofR and SofR
        String RofS = "";
        String SofS = "";
        int borderIndexOfS = -1;

        //r + (r + s) && r + (s + r)
        //aka +(r, +(r, s))
        if( S.charAt(0) == op.charAt(0) ){
            borderIndexOfS = Commons.findBorder(S);
            RofS = Commons.getR(S, borderIndexOfS);
            SofS = Commons.getS(S, borderIndexOfS);

            if( R.equals(RofS) ) { //r + (r + s)
                retRegex = regex.substring(0, orOpIndex) + S + regex.substring(orOpIndex + endOfInnerRegexIndex+1);
            } else if( R.equals(SofS) ) {  //r + (s + r)
                retRegex = regex.substring(0, orOpIndex) + S + regex.substring(orOpIndex + endOfInnerRegexIndex+1);
            }
        }

        String RofR = "";
        String SofR = "";
        int borderIndexOfR = -1;

        //(r + s) + r && (s + r) + r
        //aka +(+(r, s), r)
        if( R.charAt(0) == op.charAt(0) ){
            borderIndexOfR = Commons.findBorder(R);
            RofR = Commons.getR(R, borderIndexOfR);
            SofR = Commons.getS(R, borderIndexOfR);

            if( S.equals(RofR) ) { //(r + s) + r
                retRegex = regex.substring(0, orOpIndex) + R + regex.substring(orOpIndex + endOfInnerRegexIndex+1);
            } else if( S.equals(SofR) ) {  //(s + r) + r
                retRegex = regex.substring(0, orOpIndex) + R + regex.substring(orOpIndex + endOfInnerRegexIndex+1);
            }
        }

        if( regex.equals(retRegex) ) { //no changes occurred, we can move on
            return removeDublicatesTrackIndex(retRegex, orOpIndex+1, op);
        } else { // some changes occurred, have to recheck whole regex
            return removeDublicatesTrackIndex(retRegex, 0, op);
        }
    }

    private DupeRemove(){}
}