package gitRegexDeriv.src.DFA.similarityOps;

import gitRegexDeriv.src.main.Commons;

public class EpsylonRemove {
    public static String rewrite(String regex){
        //ε · r ≈ r
        //r · ε ≈ r

        String retRegex = regex;

        retRegex = rewriteRorSIndex(retRegex, 0);
        //retRegex = moveBracketsTrackIndex(retRegex, 0, "°");

            return retRegex;
    }

    private static String rewriteRorSIndex(String regex, int index) {
        int orOpIndex = regex.indexOf("°", index);

        if(orOpIndex == -1) return regex;

        int endOfInnerRegexIndex = Commons.findEndOfRegex( regex.substring(orOpIndex) );
        String remainingRegex = regex.substring( orOpIndex, orOpIndex+endOfInnerRegexIndex+1 );
        int borderIndex = Commons.findBorder( remainingRegex );

        String R = Commons.getR(remainingRegex, borderIndex);
        String S = Commons.getS(remainingRegex, borderIndex);

        String retRegex = regex;

        //TODO figure out if there's nothing to do
        //aka don't rewrite stuff, if it's already in +(r, +(s, t)) format

        //R's mainop is op
        if( R.equals("ε") ){
            retRegex = regex.substring(0, orOpIndex) + S + regex.substring(orOpIndex + endOfInnerRegexIndex+1);
        } else if ( S.equals("ε") ){
            retRegex = regex.substring(0, orOpIndex) + R + regex.substring(orOpIndex + endOfInnerRegexIndex+1);
        }

        if( regex.equals(retRegex) ) { //no changes occurred, we can move on
            return rewriteRorSIndex(retRegex, orOpIndex+1);
        } else { // some changes occurred, check again, if we can move more stuff out
            return rewriteRorSIndex(retRegex, 0);
        }

    }

    private EpsylonRemove(){};
}
