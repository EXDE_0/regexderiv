package gitRegexDeriv.src.DFA.similarityOps;

import gitRegexDeriv.src.main.Commons;

public class SingleOperandRemove {

    public static String rewrite(String regex){
        //(r∗)∗ ≈ r∗

        String retRegex = regex;

        retRegex = rewriteDupeIterateIndex(retRegex, 0, "*");
        retRegex = rewriteDupeIterateIndex(retRegex, 0, Commons.NEGATION);
        //retRegex = moveBracketsTrackIndex(retRegex, 0, "°");

            return retRegex;
    }

    private static String rewriteDupeIterateIndex(String regex, int index, String op) {
        int orOpIndex = regex.indexOf(op, index);

        if(orOpIndex == -1) return regex;

        int endOfInnerRegexIndex = Commons.findEndOfRegex( regex.substring(orOpIndex) );
        String R = regex.substring(orOpIndex+2, orOpIndex + endOfInnerRegexIndex);

        String retRegex = regex;

        //R's mainop is op
        if( R.charAt(0) == op.charAt(0) ){
            retRegex = regex.substring(0, orOpIndex) + R + regex.substring(orOpIndex + endOfInnerRegexIndex+1);
        }

        if( regex.equals(retRegex) ) { //no changes occurred, we can move on
            return rewriteDupeIterateIndex(retRegex, orOpIndex+1, op);
        } else { // some changes occurred, check again, if we can move more stuff out
            return rewriteDupeIterateIndex(retRegex, orOpIndex, op);
        }

    }

    SingleOperandRemove(){};
}
