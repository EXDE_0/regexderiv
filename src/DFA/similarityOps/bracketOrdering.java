package gitRegexDeriv.src.DFA.similarityOps;

import gitRegexDeriv.src.main.Commons;

public final class bracketOrdering {

    public static String moveBrackets(String regex) {
        String retRegex = regex;

        retRegex = moveBracketsTrackIndex(retRegex, 0, "+");
        retRegex = moveBracketsTrackIndex(retRegex, 0, "°");
        retRegex = moveBracketsTrackIndex(retRegex, 0, "&");

        return retRegex;
    }

    private static String moveBracketsTrackIndex(String regex, int index, String op) {
        int orOpIndex = regex.indexOf(op, index);

        if(orOpIndex == -1) return regex;

        int endOfInnerRegexIndex = Commons.findEndOfRegex( regex.substring(orOpIndex) );
        String remainingRegex = regex.substring( orOpIndex, orOpIndex+endOfInnerRegexIndex+1 );
        int borderIndex = Commons.findBorder( remainingRegex );

        String R = Commons.getR(remainingRegex, borderIndex);
        String S = Commons.getS(remainingRegex, borderIndex);

        String retRegex = regex;

        //TODO figure out if there's nothing to do
        //aka don't rewrite stuff, if it's already in +(r, +(s, t)) format

        //R's mainop is op
        if( R.charAt(0) == op.charAt(0) ){
            int borderIndexOfR = Commons.findBorder(R);
            String RofR = Commons.getR(R, borderIndexOfR);
            String SofR = Commons.getS(R, borderIndexOfR);

            //retregex = unprocessed regex + r + ", +(s, t)" + rest of the unprocessed regex
            retRegex = regex.substring(0, orOpIndex+2) +
                        RofR +
                        ", " + op + "(" + SofR + ", " + S + ")" +
                        regex.substring(orOpIndex + endOfInnerRegexIndex); //orOpindex + how long is the orOpindex regex
        }

        if( regex.equals(retRegex) ) { //no changes occurred, we can move on
            return moveBracketsTrackIndex(retRegex, orOpIndex+1, op);
        } else { // some changes occurred, check again, if we can move more stuff out
            return moveBracketsTrackIndex(retRegex, orOpIndex, op);
        }
    }

    private bracketOrdering(){}
}
