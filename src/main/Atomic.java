package gitRegexDeriv.src.main;

public class Atomic {

    public enum AtomicType {
        CHARACTER, SUBREGEX
    };

    private int ID = -1;
    private int parentID = -1;
    private AtomicType type;
    private int index = -1; // -1 if invalid
    private int size = -1; //subregex size, -1 if invalid
    private String parentOperand = ""; // empty string if invalid

    public Atomic(AtomicType type, int index, int size, String parentOperand) {
        this.type = type;
        this.index = index;
        this.size = size;
        this.parentOperand = parentOperand;
    }

    public Atomic(int id, AtomicType type, int index, int size, String parentOperand) {
        this.ID = id;
        this.type = type;
        this.index = index;
        this.size = size;
        this.parentOperand = parentOperand;
    }

    public Atomic(int id, int parentID, AtomicType type, int index, int size, String parentOperand) {
        this.ID = id;
        this.parentID = parentID;
        this.type = type;
        this.index = index;
        this.size = size;
        this.parentOperand = parentOperand;
    }

    @Override
    public boolean equals(Object otherAtomic) {
        if (this == otherAtomic) return true;
        if (otherAtomic == null || getClass() != otherAtomic.getClass()) return false;

        Atomic that = (Atomic) otherAtomic;
        return this.type.equals(that.type)
            && this.index == that.index
            && this.size == that.size
            && this.parentOperand.equals(that.parentOperand);
    }

    @Override
    public String toString() {
        return "ID: " + ID + "\nparentID: " + parentID + "\nType: " + type.name() + "\nIndex: " +
            index + "\nSize: " + size + "\nParent op: " + parentOperand ;
    }

    public int getID() {
        return ID;
    }

    public void setID(int id) {
        this.ID = id;
    }

    public int getParentId() {
        return this.parentID;
    }

    public void setParentId(int parentId) {
        this.parentID = parentId;
    }

    public AtomicType getType() {
        return type;
    }
    public void setType(AtomicType type) {
        this.type = type;
    }
    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
    public int getSize() {
        return size;
    }
    public void setSize(int size) {
        this.size = size;
    }
    public String getParentOperand() {
        return parentOperand;
    }
    public void setParentOperand(String parentOperand) {
        this.parentOperand = parentOperand;
    }
}
