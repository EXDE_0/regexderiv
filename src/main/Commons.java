package gitRegexDeriv.src.main;

public final class Commons {

    final public static String ops = "+ & ° * ¬";
    final public static String specialCharacters = "(), ".concat(Commons.ops.replace(" ", ""));
    final public static String NEGATION = "¬";
    final public static String EPSILON = "ε";
    final public static String NULLSET = "∅";
    private static int atomicIDStore = -1;
    private static int nodeIDStore = -1;

    // returns the exact index of where the comma is after
    // r ends
    public static int findBorder(String regex) {
        int openBrackets = 0;
        int closedBrackets = 0;
        final int regLen = regex.length();

        String firstSym = regex.substring(2,3); //what is r?
        if ( ops.contains(firstSym) ) {

            for ( int i = 0; i < regLen; i++) {
                if ( regex.charAt(i) == '(' ) openBrackets += 1;
                if ( regex.charAt(i) == ')' ) closedBrackets += 1;

                if ( openBrackets - closedBrackets == 1
                    && closedBrackets > 0
                    && i != 1
                    && i+1 != regLen )
                    return i+1;

                if ( i + 1 == regLen && openBrackets - closedBrackets == 0) return regex.indexOf(",", 0);
            }
        } else {
            return regex.indexOf(",", 0);
        }

        return -1; //error
    }

    public static int findEndOfRegex( String regex ) {
        int openBrackets = 0;
        int closedBrackets = 0;
        final int regLen = regex.length();

        for ( int i = 0; i < regLen; i++) {
                if ( regex.charAt(i) == '(' ) openBrackets += 1;
                if ( regex.charAt(i) == ')' ) closedBrackets += 1;
                if ( openBrackets - closedBrackets == 0 && i != 0 ) return i;
        }

        return -1; //error
     }

    //lob off first part of regex and s, TODO better matching for r
    public static String getR( String regex, int borderIndex) {
        if ( regex.substring(0, 1).equals("*") || regex.charAt(0) == Commons.NEGATION.charAt(0) ) return regex.substring(2, regex.length()-1);
        else return regex.substring(2, borderIndex);
    }

    public static String getS( String regex, int borderIndex) {
        //if ( regex.substring(borderIndex+2, borderIndex+3).equals("*") ) return regex.substring(borderIndex+4, regex.length() - 1);
        return regex.substring(borderIndex+2, regex.length() - 1);
    }

    public static int getAtomicID(){
        ++atomicIDStore;
        return atomicIDStore;
    }

    public static int getNodeID() {
        ++nodeIDStore;
        return nodeIDStore;
    }

    private Commons() {}
}
