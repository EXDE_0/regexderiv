package gitRegexDeriv.src.main;

import java.util.List;

public class DerivationOps {

    RegexOperations regOps = RegexOperations.getInstance();

    public String derive(String symbol, String regex, List<String> alphabet) {

        if ( !(alphabet.contains(symbol)) ) return "∅"; //return, symbol not part of alphabet
        if ( regex.length() <= 1 ) return der(symbol, regex); //regex only contains a symbol

        String mainOp = String.valueOf(regex.charAt(0));
        String r = "";
        String s = "";

        if ( !mainOp.equals("*") && !mainOp.equals(Commons.NEGATION) ){
            int borderIndex = Commons.findBorder(regex);

            r = Commons.getR(regex, borderIndex);
            s = Commons.getS(regex, borderIndex);
        } else { r = regex.substring(2, regex.length() - 1); }

        String changedR = "";        //only used if r gets derived
        String changedS = "";       // only used if s gets derived
        String nullableResult = ""; //not used unless nullable was called

        //derive based on operation
        switch (mainOp) {
            case "+":
                changedR = derive(symbol, r, alphabet);
                changedS = derive(symbol, s, alphabet);
                //regex = String.format("+(%s, %s)", changedR, changedS);
                break;
            case "°":
                //String[] resultCon = derCon(r, s);
                changedR = derive(symbol, r, alphabet);
                changedS = derive(symbol, s, alphabet);
                nullableResult = nullableFull(r,alphabet);
                //regex = String.format("+(°(%s, %s), °(%s, %s))", changedR, s, nullableResult, changedS);
                break;

            case "&":
                changedR = derive(symbol, r, alphabet);
                changedS = derive(symbol, s, alphabet);
                //regex = String.format("&(%s, %s)", changedR, changedS);
                break;

            case "*":
                changedR = derive(symbol, r, alphabet);
                //r = String.format("°(%s, %s)", changedR, regex);
                break;

            case Commons.NEGATION:
                changedR = derive(symbol, r, alphabet);
                //r = String.format("°(%s, %s)", changedR, regex);
                break;

            default:
                regex = der(symbol, r);
                return regex;
        }

        switch (mainOp) {
            case "+":
                regex = regOps.doOr(changedR, changedS);
                break;
            case "°":
                String left = regOps.doCon(changedR, s);
                String right = regOps.doCon(nullableResult, changedS);
                regex = regOps.doOr(left, right);
                break;

            case "&":
                regex = regOps.doAnd(changedR, changedS);
                break;

            case "*":
                regex = regOps.doCon(changedR, regex);
                break;

            case Commons.NEGATION:
                regex = regOps.doNeg( changedR ) ;
                break;
            default:
                break;
        }

        return regex;
    }

    private String der(String symbol, String r) {
        if (symbol.equals(r) ){
            return "ε";
        } else return "∅";
    }

    public String nullableFull( String regex, List<String> alphabet ){
        if( regex.equals("ε")) return "ε";
        if( regex.equals("∅") ) return "∅"; //returns empty set, when regex is not matching //TODO check this more?
        if( regex.length() == 1 && alphabet.contains(regex) ) return "∅"; //symbol

        String mainOp = String.valueOf(regex.charAt(0));
        if( mainOp.equals("*") ) return "ε"; //iterating

        int borderIndex = Commons.findBorder(regex);
        String r = Commons.getR(regex, borderIndex);
        String s = !mainOp.equals(Commons.NEGATION) ? Commons.getS(regex, borderIndex) : "";

        switch (mainOp) {
            case "+":
                if( nullableFull(r, alphabet).concat(nullableFull(s, alphabet)).contains("ε") ){
                    return "ε";
                };
                break;

            case "°":
                if( nullableFull(r, alphabet).concat(nullableFull(s, alphabet)).equals("εε") ){
                    return "ε";
                };
                break;

            case "&":
                if( nullableFull(r, alphabet).concat(nullableFull(s, alphabet)).equals("εε") ){
                    return "ε";
                };
                break;

            case Commons.NEGATION:
                if( nullableFull(r, alphabet).equals(Commons.EPSILON) ) { return Commons.NULLSET; }
                else if ( nullableFull(r, alphabet).equals(Commons.NULLSET) ) { return Commons.EPSILON; }
                else { System.err.println("Nullable returned unknown char"); }
                break;

            default:
                break;
        }

        return "∅";
    }

}