package gitRegexDeriv.src.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class InfixToPrefix {

    public static String rewrite(String input) {
        String[] ops = Commons.ops.split(" ");
        ArrayList<String> operators = new ArrayList<>();
        for (var o : ops) {
            operators.add(o);
        }
        return reverseExpression(printChars(addBrackets(infixToPostfix(reverseExpression(input)), 0, operators)));
    }

    private static String printChars(List<String> chars) {
        StringBuilder output = new StringBuilder();

        for (var c : chars) {
            output.append(c);
        }
        return output.toString();
    }

    // Function to return precedence of operators
    private static int prec(char c) {
        if (c == '*' || c == '¬')
            return 3;
        else if (c == '°')
            return 2;
        else if (c == '+' || c == '&')
            return 1;
        else
            return -1;
    }

    private static String reverseExpression(String exp) {
        char[] tmp_exp = exp.toCharArray();
        char[] retExp = new char[exp.length()];
        int index = 0;
        for (int i = exp.length() - 1; i >= 0; i--) {
            index = exp.length() - i - 1;
            if (tmp_exp[i] == '(') {
                retExp[index] = ')';
            } else if (tmp_exp[i] == ')') {
                retExp[index] = '(';
            } else {
                retExp[index] = tmp_exp[i];
            }
        }
        return new String(retExp);
    }

    // Function to perform infix to postfix conversion
    private static ArrayList<String> infixToPostfix(String s) {
        Stack<Character> st = new Stack<>();
        String operand = "";
        ArrayList<String> result = new ArrayList<>();

        for (int i = 0; i < s.length(); i++) {
            Character c = s.charAt(i);

            // If the scanned character is
            // an operand, add it to the output string.
            if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == 'ε') {
                operand += c;
            }

            // If the scanned character is
            // an ‘(‘, push it to the stack.
            else if (c == '(')
                st.push('(');

            // If the scanned character is an ‘)’,
            // pop and add to the output string from the stack
            // until an ‘(‘ is encountered.
            else if (c == ')') {
                while (st.peek() != '(') {
                    if (operand != "")
                        result.add(operand);
                    operand = "";
                    result.add(st.pop().toString());
                }
                st.pop();
            }

            // If an operator is scanned
            else {
                while (!st.isEmpty() && (prec(c) < prec(st.peek()))) { // || prec(c) == prec(st.peek())
                    if (operand != "")
                        result.add(operand);
                    operand = "";
                    result.add(st.pop().toString());
                }
                if (operand != "")
                    result.add(operand);
                operand = "";
                st.push(c);
            }
        }

        if (operand != "")
            result.add(operand);
        // Pop all the remaining elements from the stack
        while (!st.isEmpty()) {
            // result.add(")");
            result.add(st.pop().toString());
        }

        return result;
    }

    private static ArrayList<String> addBrackets(ArrayList<String> postFixOg, int position,
            ArrayList<String> operators) {

        ArrayList<String> retString = postFixOg;
        int min = postFixOg.size() + 1;

        for (var op : operators) {
            int index = postFixOg.subList(position, postFixOg.size()).indexOf(op);
            if (index < min && index > -1) {
                min = index;
            }
        }
        min = min + position;

        if (min >= postFixOg.size()) {
            return postFixOg;
        }

        // detect 2 or 1 operand operator

        if (min < postFixOg.size()) {
            // detect 2 or 1 operand operator
            if (postFixOg.get(min).equals("*") || postFixOg.get(min).equals("¬")) {
                // [ a, * ] -> [ (, a, * ] -> [ (, a, ), * ]
                retString.add(min - 1, "("); // 1 if it's single op
                retString.add(min + 1, ")");
                // merge strings
                // [ (, a, ), * ] -> [ a, ), * ] -> [ ), * ] -> [ * ] -> [ ] -> [ (a)* ]
                String tmp = printChars(postFixOg.subList(min - 1, min + 3));
                retString.remove(min - 1);
                retString.remove(min - 1);
                retString.remove(min - 1);
                retString.remove(min - 1);
                retString.add(min - 1, tmp);
            } else {
                // 2 ops
                retString.add(min - 2, "("); // 1 if it's single op
                retString.add(min, " ,");
                retString.add(min + 2, ")");
                // merge strings
                String tmp = printChars(postFixOg.subList(min - 2, min + 4));
                retString.remove(min - 2);
                retString.remove(min - 2);
                retString.remove(min - 2);
                retString.remove(min - 2);
                retString.remove(min - 2);
                retString.remove(min - 2);
                retString.add(min - 2, tmp);
            }
        }
        retString = addBrackets(retString, min - 1, operators);

        return retString;
    }

    private InfixToPrefix() {
    };
}