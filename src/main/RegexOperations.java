package gitRegexDeriv.src.main;

public class RegexOperations {

    private static RegexOperations single_instance = null;
    final String NULLSET = Commons.NULLSET;
    final String EPSILON = Commons.EPSILON;

    public static synchronized RegexOperations getInstance() {
        if ( single_instance == null )
            single_instance = new RegexOperations();
        return single_instance;
    }

    private RegexOperations(){};

    public String doOr( String r, String s ){
        if ( r.equals(NULLSET) && s.equals(NULLSET) ) return NULLSET; //1
        if ( r.equals(NULLSET) ) return s;                            //2
        if ( s.equals(NULLSET) ) return r;                            //2
        if ( r.equals(EPSILON) && s.equals(EPSILON) ) return EPSILON; //1
        else return String.format("+(%s, %s)", r, s);                 //3
    }

    public String doCon( String r, String s ) {
        if ( r.equals(NULLSET) || s.equals(NULLSET) ) return NULLSET; //4
        if ( r.equals(EPSILON) && s.equals(EPSILON) ) return EPSILON; //1
        if ( r.equals(EPSILON) ) return s;                            //1
        if ( s.equals(EPSILON) ) return r;                            //1
        else return String.format("°(%s, %s)", r, s);                 //2
    }

    public String doAnd( String r, String s ) {
        if ( r.equals(NULLSET) || s.equals(NULLSET) ) return NULLSET;
        if ( r.equals(EPSILON) && s.equals(EPSILON) ) return EPSILON;
        else return String.format("&(%s, %s)", r, s);
    }

    public String doItr( String r ) {
        if ( r.equals(NULLSET) ) return EPSILON;
        if ( r.equals(EPSILON) ) return EPSILON;
        else return String.format("*(%s)", r);
    }

    public String doNeg ( String r ) {
        if ( r.equals(NULLSET) ) return EPSILON;
        if ( r.equals(EPSILON) ) return NULLSET;
        else return String.format("%s(%s)", Commons.NEGATION, r);
    }

    //R = NULL, S = REGEX
    //R = REGEX, S = NULL
    //R = EPSILON, S = REGEX
    //R = REGEX, S = EPSILON

    //R = EPSILON, S = NULL
    //R = NULL, S = EPSILON

    //R = NULL, S = NULL
    //R = EPSILON, S = EPSILON
}   //R = REGEX, S = REGEX
