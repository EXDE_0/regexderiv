package gitRegexDeriv.src.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import gitRegexDeriv.src.DFA.Automaton;
import gitRegexDeriv.src.DFA.DFABuilder;
import gitRegexDeriv.src.DFA.runDFA;
import gitRegexDeriv.src.DFA.similarityOps.AlphabeticalOrdering;
import gitRegexDeriv.src.DFA.similarityOps.DupeRemove;
import gitRegexDeriv.src.DFA.similarityOps.SingleOperandRemove;
import gitRegexDeriv.src.DFA.similarityOps.bracketOrdering;
import gitRegexDeriv.src.main.Atomic.AtomicType;
import gitRegexDeriv.src.testing.findAtomicsUnitTest;

class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputRegex = scanner.nextLine();
        List<String> alphabet = getAlphabet(inputRegex);
        String inputTransformed = getInput(inputRegex);

        System.out.println(alphabet);
        System.out.println(inputTransformed);

        Automaton automaton = new DFABuilder().buildDFA(inputTransformed, alphabet);

        System.out.println(automaton);

        String inputWord = scanner.nextLine();

        while(!inputWord.equals("qq")){
            System.out.println(runDFA.execute(inputWord, automaton));

            inputWord = scanner.nextLine();
        }

        //testDeriving();
        //testFullDerivation();
        //testAlphabeticalOrdering();

        //testRebuildRegex();
        //testDuplicates();
        //testmoveBrackets();
        //testSingleOperandRemove();
        //testRewriteChain();
        //testDFABuilder();
        //testrunDFA();
        //testAlphabet();
    }

    public static List<String> getAlphabet(String inputRegex){
        String usedCharacters = "( ) * + ° ¬ ε ∅ & ,";
        ArrayList<String> alphabet = new ArrayList<>();

        for( int i = 0; i < inputRegex.length(); i++ ){
            String letter = String.valueOf( inputRegex.charAt(i) );

            if( !usedCharacters.contains( letter ) && !alphabet.contains( letter ) ) {
                alphabet.add(letter);
            }
        }

        return alphabet;
    }

    public static String getInput(String input) {
        return InfixToPrefix.rewrite(input);
    }

    public static void testDeriving(){
        Boolean investigate_multiple = false;
        List<String> alphabet = new ArrayList<String>();
        alphabet.add("a"); alphabet.add("b"); alphabet.add("c");
        alphabet.add("d"); alphabet.add("g"); alphabet.add("j");
        alphabet.add("o");alphabet.add("r");alphabet.add("x");alphabet.add("z");
        alphabet.add("0"); alphabet.add("1");



        String[] input = new String[15];
        input[0] = "+(°(a, b), °(a, c))"; //WORKS
        input[1] = "°(a, b)"; //WORKS
        input[2] = "+(a, b)"; //WORKS
        input[3] = "°(°(a, b), °(a, c))"; //WORKS
        input[4]  = "*(a)"; //WORKS
        input[5] = "*(+(a, *(b)))"; //WORKS
        input[6] = "*(°(*(a), b))"; //Doesn't calculate fully,
        //returns (°(°(*(a), b), *(°(*(a), b)))) which is correct, but calculation incomplete
        //second calculation shows, that it might actually be correct
        //further ivestigation is needed
        input[7] = "+(+(a, b), °(°(a, c), °(a, a)))"; //WORKS
        input[8] = "a"; //WORKS
        input[9] = "+(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r))))";
        input[10] = "*(°(*(+(0, 1)), *(+(0, 1))))";
        input[11] = "*(°(*(°(0, 1)), *(°(0, 1))))";
        input[12] = "*(°(*(°(1, 0)), *(°(1, 0))))";
        input[13] = "¬(a)"; //OUT OF BOUNDS EXCEPTION TODO IMPLEMENT NEGATION
        input[14] = "¬(+(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r)))))";

        String inputSingle = input[14];

        String result = "";
        if (investigate_multiple) {
            try {
                for (String inp : input) {
                    result = new DerivationOps().derive("a", inp, alphabet);
                    System.out.println("In: " + inp + " Out: " + result);
                }
            } catch(StackOverflowError t) {
                System.out.println("Caught "+t);
                //t.printStackTrace();
            }
        } else {
            try {
                    result = new DerivationOps().derive("o", inputSingle, alphabet);
                    System.out.println("In: " + inputSingle + " Out: " + result);
                }
            catch(StackOverflowError t) {
                System.out.println("Caught "+t);
                //t.printStackTrace();
            }
        }
    }

    public static void testFullDerivation(){
        List<String> alphabet = new ArrayList<String>();
        alphabet.add("a"); alphabet.add("b"); alphabet.add("c");
        alphabet.add("j"); alphabet.add("g"); alphabet.add("x");
        alphabet.add("0"); alphabet.add("1");

        String inputRE = "+(+(a, b), °(°(a, c), °(a, a)))";
        String inputRE2 = "+(+(+(a, b), +(a, b)), +(+(d, x), °(+(j, g), +(a, b))))";
        String inputRE3 = "+(a, +(b, +(a, +(b, +(d, +(x, °(+(j, g), +(a, b))))))))";
        String inputRE4 = "*(°(*(a), b))";
        String inputRE5 = "*(°(*(°(0, 1)), *(°(0, 1))))";

        //String test = "+(a, +(b, °(°(a, c), °(a, a))))";
        String inputString = "1";

        String result = inputRE5;

        var deriver = new DerivationOps();

        try {
            for(int i = 0; i < inputString.length(); i++){
                result = deriver.derive(String.valueOf(inputString.charAt(i)), result, alphabet);
            }

            System.out.println("In: " + inputString + " Out: " + deriver.nullableFull(result, alphabet));
        }
        catch(StackOverflowError t) {
        System.out.println("Caught "+t);
        //t.printStackTrace();
    }
    }

    public static void testAlphabeticalOrdering(){
        //TODO write this teseting stuff
        findAtomicsUnitTest tester = new findAtomicsUnitTest();

        tester.runAllTests();
        tester.runAllTestsSort();
        //tester.runOneSortTest(13);

        String newInput = "°(+(°(a, +(c, b)), c), +(°(x, z), +(d, g)))";
        //newInput = "+(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r))))";
        ArrayList<ArrayList<Atomic>> newExp = new ArrayList<>() {{
            add( new ArrayList<>() {{
                add( new Atomic(AtomicType.SUBREGEX, 0, 43, "°") );
            }});
            add( new ArrayList<>() {{
                add( new Atomic(AtomicType.SUBREGEX, 4, 13, "+") );
                add( new Atomic(AtomicType.CHARACTER, 19, 1, "+") );//r
                add( new Atomic(AtomicType.SUBREGEX, 25, 7, "+") );
                add( new Atomic(AtomicType.CHARACTER, 36, 1, "+") );
                add( new Atomic(AtomicType.CHARACTER, 39, 1, "+") );//s
            }});
            add(new ArrayList<>() {{
                add( new Atomic(AtomicType.CHARACTER, 11, 1, "+") );
                add( new Atomic(AtomicType.CHARACTER, 14, 1, "+") );//r
            }});
         }};

         String newInput2 = "°(+(°(a, +(c, b)), c), °(+(x, z), +(d, g)))";
         ArrayList<ArrayList<Atomic>> newExp2 = new ArrayList<>() {{
            add( new ArrayList<>() {{
                add( new Atomic(AtomicType.SUBREGEX, 0, 43, "°") );
            }});
            add( new ArrayList<>() {{
                add( new Atomic(AtomicType.SUBREGEX, 4, 13, "+") );
                add( new Atomic(AtomicType.CHARACTER, 19, 1, "+") );//r
                add( new Atomic(AtomicType.SUBREGEX, 23, 19, "°") );
            }});
            add(new ArrayList<>() {{
                add( new Atomic(AtomicType.CHARACTER, 11, 1, "+") );
                add( new Atomic(AtomicType.CHARACTER, 14, 1, "+") );//r
            }});
            add(new ArrayList<>() {{
                add( new Atomic(AtomicType.CHARACTER, 27, 1, "+") );
                add( new Atomic(AtomicType.CHARACTER, 30, 1, "+") );
                add( new Atomic(AtomicType.CHARACTER, 36, 1, "+") ); //should be on anoter level
                add( new Atomic(AtomicType.CHARACTER, 39, 1, "+") );
            }});
         }};

         String newInput3 = "°(+(x, z), +(d, g))";

         String newInput4 = "+(°(a, b), °(a, c))";
         //String newInput4 = "+(°(a, c), °(a, b))";

         String newinput5 = "°(°(°(a, b), °(d, °(d, °(d, f)))), °(°(a, b), °(z, k)))";

         var newInput6 = "+(+(+(d, x), +(g, j)), +(+(a, b), °(+(c, z), +(o, r))))";

        //tester.runAllTests();
        //tester.runOneTest(1);
        //tester.runOneTest(newInput, newExp);

        var result = AlphabeticalOrdering.findatomicsExtendedAT(newInput, String.valueOf(newInput.charAt(0)), 0);

        var result2 = AlphabeticalOrdering.findatomicsExtendedAT(newInput2, String.valueOf(newInput2.charAt(0)), 0);

        var result4 = AlphabeticalOrdering.findatomicsExtendedAT(newInput4, String.valueOf(newInput4.charAt(0)), 0);

        var result5 = AlphabeticalOrdering.findatomicsExtendedAT(newinput5, String.valueOf(newinput5.charAt(0)), 0);

        var result6 = AlphabeticalOrdering.findatomicsExtendedAT(newInput6, String.valueOf(newInput6.charAt(0)), 0);

        var sortedMate = AlphabeticalOrdering.sortRegex(result4.get(0), newInput4);

        //ArrayList<String> result2 = new ArrayList<>();
        for (var r : result5){
            //result2 = AlphabeticalOrdering.flatten(r, newInput);
            //System.out.println(AlphabeticalOrdering.flatten(r, newInput4));
            //System.out.println(sortedMate);
            //System.out.println(AlphabeticalOrdering.flatten(r, newInput2));
            System.out.println(r);
        }

        //System.out.println( AlphabeticalOrdering.sortRegex(newExp.get(1), newInput));
    }

    public static void testRebuildRegex(){
        String newInput = "+(+(z, x), +(g, d))";
        String newInput2 = "°(+(z, x), +(g, d))";
        String newInput3 = "+(°(a, c), °(a, b))";
        String newInput4 = "+(+(+(a, b), +(c, z)), +(+(d, x), °(+(j, g), +(r, o))))";
        String newInput5 = "°(+(z, x), +(z, d))";
        String newInput6 = "+(+(+(a, b), +(c, ¬(z))), +(+(d, x), °(+(g, j), +(o, r))))";

        var result = AlphabeticalOrdering.findatomicsExtendedAT(newInput, String.valueOf(newInput.charAt(0)), 0);
        var result2 = AlphabeticalOrdering.findatomicsExtendedAT(newInput2, String.valueOf(newInput2.charAt(0)), 0);
        var result3 = AlphabeticalOrdering.findatomicsExtendedAT(newInput3, String.valueOf(newInput3.charAt(0)), 0);
        var result4 = AlphabeticalOrdering.findatomicsExtendedAT(newInput4, String.valueOf(newInput4.charAt(0)), 0);
        var result5 = AlphabeticalOrdering.findatomicsExtendedAT(newInput5, String.valueOf(newInput5.charAt(0)), 0);
        var result6 = AlphabeticalOrdering.findatomicsExtendedAT(newInput6, String.valueOf(newInput6.charAt(0)), 0);

        var regexResult = AlphabeticalOrdering.rebuildRegex(result, newInput);
        var regexResult2 = AlphabeticalOrdering.rebuildRegex(result2, newInput2);
        var regexResult3 = AlphabeticalOrdering.rebuildRegex(result3, newInput3);
        var regexResult4 = AlphabeticalOrdering.rebuildRegex(result4, newInput4);
        var regexResult5 = AlphabeticalOrdering.rebuildRegex(result5, newInput5);
        var regexResult6 = AlphabeticalOrdering.rebuildRegex(result6, newInput6);

        System.out.println(regexResult);
        System.out.println(regexResult2);
        System.out.println(regexResult3);
        System.out.println(regexResult4);
        System.out.println(regexResult5);
        System.out.println(regexResult6);
    }

    public static void testDuplicates(){
        String newInput = "+(+(z, x), +(g, d))";
        String newInput2 = "°(+(z, x), +(x, z))";
        String newInput3 = "+(°(a, c), °(a, c))";
        String newInput4 = "+(e, e)";
        String newInput5 = "°(+(r, r), t)";
        String newInput6 = "+(+(+(a, b), +(a, b)), +(+(d, x), °(+(j, g), +(a, b))))";
        String newInput7 = "+(+(e, +(e, +(e, e))), +(e, a))";
        String newInput8 = "+(+(e, +(a, +(e, e))), +(e, e))";

        var result = DupeRemove.removeDublicates(newInput);
        var result2 = DupeRemove.removeDublicates(newInput2);
        var result3 = DupeRemove.removeDublicates(newInput3);
        var result4 = DupeRemove.removeDublicates(newInput4);
        var result5 = DupeRemove.removeDublicates(newInput5);
        var result6 = DupeRemove.removeDublicates(newInput6);
        var result7 = DupeRemove.removeDublicates(newInput7);
        var result8 = DupeRemove.removeDublicates(newInput8);

        System.out.println(result);
        System.out.println(result2);
        System.out.println(result3);
        System.out.println(result4);
        System.out.println(result5);
        System.out.println(result6);
        System.out.println(result7);
        System.out.println(result8);
    }

    public static void testmoveBrackets(){
        String newInput = "+(+(z, x), +(g, d))";
        String newInput2 = "°(+(z, x), +(x, z))";
        String newInput3 = "+(°(a, c), °(a, c))";
        String newInput4 = "+(e, e)";
        String newInput5 = "°(+(r, r), t)";
        String newInput6 = "+(+(+(a, b), +(c, b)), +(+(d, x), °(+(j, g), +(a, b))))";
        String newInput7 = "+(+(0, 1), +(1, 0))";
        String newInput8 = "*(°(*(+(0, 1)), *(+(0, 1))))";

        //var result = bracketOrdering.moveBrackets(newInput);
        //var result2 = bracketOrdering.moveBrackets(newInput2);
        //var result3 = bracketOrdering.moveBrackets(newInput3);
        //var result4 = bracketOrdering.moveBrackets(newInput4);
        //var result5 = bracketOrdering.moveBrackets(newInput5);
        //var result6 = bracketOrdering.moveBrackets(newInput6);
        var result7 = bracketOrdering.moveBrackets(newInput7);
        var result8 = bracketOrdering.moveBrackets(newInput8);

        //System.out.println(result);
        //System.out.println(result2);
        //System.out.println(result3);
        //System.out.println(result4);
        //System.out.println(result5);
        //System.out.println(result6);
        System.out.println(result7);
        System.out.println(result8);
    }

    public static void testSingleOperandRemove(){
        String newInput = "*(+(*(a), *(b)))";
        String newInput2 = "*(°(*(°(0, 1)), *(°(0, 1))))";
        String newInput3 = "¬(°(*(°(0, 1)), *(°(0, 1))))";

        var result = SingleOperandRemove.rewrite(newInput);
        var result3 = SingleOperandRemove.rewrite(newInput3);

        System.out.println(result);
        System.out.println(result3);
    }

    public static void testRewriteChain(){
        String newInput = "+(+(z, x), +(g, d))";
        String newInput2 = "°(+(z, x), +(x, z))"; //fail here?
        String newInput3 = "+(°(a, c), °(a, c))";
        String newInput4 = "+(e, e)";
        String newInput5 = "°(+(r, r), t)";
        String newInput6 = "+(+(+(a, b), +(a, b)), +(+(d, x), °(+(j, g), +(a, b))))";
        String newInput7 = "+(+(0, 1), +(1, 0))";
        String newInput8 = "*(°(*(+(0, 1)), *(+(0, 1))))";

        var result = DupeRemove.removeDublicates(newInput);
        var result2 = DupeRemove.removeDublicates(newInput2);
        var result3 = DupeRemove.removeDublicates(newInput3);
        var result4 = DupeRemove.removeDublicates(newInput4);
        var result5 = DupeRemove.removeDublicates(newInput5);
        var result6 = DupeRemove.removeDublicates(newInput6);
        var result7 = DupeRemove.removeDublicates(newInput7);
        var result8 = DupeRemove.removeDublicates(newInput8);

        result = bracketOrdering.moveBrackets(result);
        result2 = bracketOrdering.moveBrackets(result2);
        result3 = bracketOrdering.moveBrackets(result3);
        result4 = bracketOrdering.moveBrackets(result4);
        result5 = bracketOrdering.moveBrackets(result5);
        result6 = bracketOrdering.moveBrackets(result6);
        result7 = bracketOrdering.moveBrackets(result7);
        result8 = bracketOrdering.moveBrackets(result8);

        result = AlphabeticalOrdering.sort(result);
        result2 = AlphabeticalOrdering.sort(result2);
        result3 = AlphabeticalOrdering.sort(result3);
        result4 = AlphabeticalOrdering.sort(result4);
        result5 = AlphabeticalOrdering.sort(result5);
        result6 = AlphabeticalOrdering.sort(result6);
        result7 = AlphabeticalOrdering.sort(result7);
        result8 = AlphabeticalOrdering.sort(result8);

        System.out.println(result);
        System.out.println(result2);
        System.out.println(result3);
        System.out.println(result4);
        System.out.println(result5);
        System.out.println(result6);
        System.out.println(result7);
        System.out.println(result8);
    }

    public static void testDFABuilder() {

        String newInput = "+(+(z, x), +(g, d))";
        String newInput2 = "°(+(z, x), +(x, z))"; //fail here?
        String newInput3 = "+(°(a, c), °(a, c))";
        String newInput4 = "+(e, e)";
        String newInput5 = "°(+(r, r), t)";
        String newInput6 = "+(+(+(a, b), +(a, b)), +(+(d, x), °(+(j, g), +(a, b))))";
        String newInput7 = "+(+(+(a, b), +(c, z)), +(+(d, x), °(+(j, g), +(r, o))))";
        String newInput8 = "+(+(0, 1), +(1, 0))";
        String newInput9 = "*(°(*(°(0, 1)), *(°(0, 1))))"; //death
        String newInput10 = "*(*(°(0, 1)))";               //works
        String newInput11 = "*(°(*(0), *(1)))";            //works
        String newInput12 = "*(°(*(°(0, 1)), *(°(2, 3))))"; //death
        String newInput13 = "¬(+(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r)))))";
        String newInput14 = "¬(*(°(*(0), *(1))))";
        String newInput15 = "*(°(°(*(°(0, 1)), *(°(2, 3))), °(*(°(4, 5)), *(°(6, 7)))))";
        String newInput16 = "*(°(*(°(°(0, 1), °(2, 3))), *(°(°(4, 5), °(6, 7)))))";
        String newInput17 = "*(°(0, 1))";



        String number = "+(0, +(1, +(2, +(3, +(4, +(5, +(6, +(7, +(8, 9)))))))))";
        String threeNums = String.format("°(°(%s, %s), %s)", number, number, number);
        String space = " ";
        String dash = "-";
        // xxx^xxx-xxxx
        String phoneNu = String.format("°(°(°(%s, ^), °(%s, -)), °(%s, %s))", threeNums, threeNums, threeNums, number);

        List<String> alphabetNewInput = new ArrayList<String>() {
            { add("z"); add("x"); add("g"); add("d"); }
        };
        List<String> alphabetNewInput2 = new ArrayList<String>() {
            { add("z"); add("x"); }
        };
        List<String> alphabetNewInput3 = new ArrayList<String>() {
            { add("a"); add("c"); }
        };
        List<String> alphabetNewInput4 = new ArrayList<String>() {
            { add("e"); }
        };
        List<String> alphabetNewInput5 = new ArrayList<String>() {
            { add("r"); add("t"); }
        };
        List<String> alphabetNewInpu6 = new ArrayList<String>() {
            { add("a"); add("b"); add("d"); add("x"); add("j");add("g"); }
        };
        List<String> alphabetNewInpu7 = new ArrayList<String>() {
            { add("a"); add("b"); add("c"); add("z"); add("d");add("x"); add("j");add("g"); add("r");add("o"); }
        };
        List<String> alphabetNewInput8 = new ArrayList<String>() {
            { add("0"); add("1"); }
        };
        List<String> alphabetNewInput9 = new ArrayList<String>() {
            { add("0"); add("1"); }
        };
        List<String> alphabetNewInput10 = new ArrayList<String>() {
            { add("0"); add("1"); }
        };
        List<String> alphabetNewInput11 = new ArrayList<String>() {
            { add("0"); add("1"); }
        };
        List<String> alphabetNewInput12 = new ArrayList<String>() {
            { add("0"); add("1"); add("2"); add("3"); }
        };
        List<String> alphabetNewInput13 = new ArrayList<String>() {
            { add("a"); add("b"); add("c"); add("z"); add("d");add("x"); add("j");add("g"); add("r");add("o"); }
        };
        List<String> alphabetNu = new ArrayList<String>() {
            { add("0"); add("1"); add("2"); add("3"); add("4"); add("5"); add("6"); add("7"); add("8"); add("9"); add("^"); add("-");}
        };
        List<String> alphabetNewInput15 = new ArrayList<String>() {
            { add("0"); add("1"); add("2"); add("3"); add("4"); add("5"); add("6"); add("7"); }
        };
        List<String> alphabetNewInput17 = new ArrayList<String>() {
            { add("0"); add("1"); }
        };


        //Automaton automaton = new DFABuilder().buildDFA(newInput, alphabetNewInput);
        //Automaton automaton2 = new DFABuilder().buildDFA(newInput2, alphabetNewInput2);
        //Automaton automaton3 = new DFABuilder().buildDFA(newInput3, alphabetNewInput3);
        //Automaton automaton4 = new DFABuilder().buildDFA(newInput4, alphabetNewInput4);
        //Automaton automaton5 = new DFABuilder().buildDFA(newInput5, alphabetNewInput5);
        //Automaton automaton6 = new DFABuilder().buildDFA(newInput6, alphabetNewInpu6);
        //Automaton automaton7 = new DFABuilder().buildDFA(newInput7, alphabetNewInpu7);
        //Automaton automaton8 = new DFABuilder().buildDFA(newInput8, alphabetNewInput8);
        //Automaton automaton9 = new DFABuilder().buildDFA(newInput9, alphabetNewInput9);
        //Automaton automaton10 = new DFABuilder().buildDFA(newInput10, alphabetNewInput10);
        //Automaton automaton11 = new DFABuilder().buildDFA(newInput11, alphabetNewInput11);
        //Automaton automaton12 = new DFABuilder().buildDFA(newInput12, alphabetNewInput12);
        //Automaton automaton13 = new DFABuilder().buildDFA(newInput13, alphabetNewInput13);
        //Automaton automaton14 = new DFABuilder().buildDFA(newInput14, alphabetNewInput11);
        //Automaton automatonPhoneNu = new DFABuilder().buildDFA(phoneNu, alphabetNu);
        //Automaton automaton15 = new DFABuilder().buildDFA(newInput15, alphabetNewInput15);
        Automaton automaton16 = new DFABuilder().buildDFA(newInput16, alphabetNewInput15);
        Automaton automaton17 = new DFABuilder().buildDFA(newInput17, alphabetNewInput17);



        //System.out.println(automaton);
        //System.out.println(automaton2);
        //System.out.println(automaton3);
        //System.out.println(automaton4);
        //System.out.println(automaton5);
        //System.out.println(automaton6);
        //System.out.println(automaton7);
        //System.out.println(automaton8);
        //System.out.println(automaton9);
        //System.out.println(automaton10);
        //System.out.println(automaton11);
        //System.out.println(automaton12);
        //System.out.println(automaton13);
        //System.out.println(automaton14);
        //System.out.println(automatonPhoneNu);
        //System.out.println(automaton15);
        System.out.println(automaton16);
        System.out.println(automaton17);
    }

    public static void testrunDFA(){
        String newInput = "+(+(z, x), +(g, d))";
        String newInput2 = "°(+(z, x), +(x, z))"; //fail here?
        String newInput3 = "+(°(a, c), °(a, c))";
        String newInput4 = "+(e, e)";
        String newInput5 = "°(+(r, r), t)";
        String newInput6 = "+(+(+(a, b), +(a, b)), +(+(d, x), °(+(j, g), +(a, b))))";
        String newInput7 = "+(+(+(a, b), +(c, z)), +(+(d, x), °(+(j, g), +(r, o))))";

        String number = "+(0, +(1, +(2, +(3, +(4, +(5, +(6, +(7, +(8, 9)))))))))";
        String threeNums = String.format("°(°(%s, %s), %s)", number, number, number);
        String space = "s";
        String dash = "-";
        // xxx xxx-xxxx
        String phoneNu = String.format("°(°(°(%s, %s), °(%s, %s)), °(%s, %s))", threeNums, space, threeNums, dash, threeNums, number);
        System.out.println(phoneNu);

        List<String> alphabetNewInput = new ArrayList<String>() {
            { add("z"); add("x"); add("g"); add("d"); }
        };
        List<String> alphabetNewInput2 = new ArrayList<String>() {
            { add("z"); add("x"); }
        };
        List<String> alphabetNewInput3 = new ArrayList<String>() {
            { add("a"); add("c"); }
        };
        List<String> alphabetNewInput4 = new ArrayList<String>() {
            { add("e"); }
        };
        List<String> alphabetNewInput5 = new ArrayList<String>() {
            { add("r"); add("t"); }
        };
        List<String> alphabetNewInpu6 = new ArrayList<String>() {
            { add("a"); add("b"); add("d"); add("x"); add("j");add("g"); }
        };
        List<String> alphabetNewInpu7 = new ArrayList<String>() {
            { add("a"); add("b"); add("c"); add("z"); add("d");add("x"); add("j");add("g"); add("r");add("o"); }
        };
        List<String> alphabetNu = new ArrayList<String>() {
            { add("0"); add("1"); add("2"); add("3"); add("4"); add("5"); add("6"); add("7"); add("8"); add("9"); add("s"); add("-");}
        };

        Automaton automaton = new DFABuilder().buildDFA(newInput, alphabetNewInput);
        Automaton automaton2 = new DFABuilder().buildDFA(newInput2, alphabetNewInput2);
        Automaton automaton3 = new DFABuilder().buildDFA(newInput3, alphabetNewInput3);
        Automaton automaton4 = new DFABuilder().buildDFA(newInput4, alphabetNewInput4);
        Automaton automaton5 = new DFABuilder().buildDFA(newInput5, alphabetNewInput5);
        Automaton automaton6 = new DFABuilder().buildDFA(newInput6, alphabetNewInpu6);
        Automaton automaton7 = new DFABuilder().buildDFA(newInput7, alphabetNewInpu7);
        Automaton automatonPhoneNu = new DFABuilder().buildDFA(phoneNu, alphabetNu);

        String newText = "z";
        String newText2 = "zx";
        String newText3 = "ac";
        String newText4 = "e";
        String newText5 = "rt";
        String newText6 = "d";
        String newText7 = "jr";

        String newTextFail = "k";
        String newText2Fail = "zx";
        String newText3Fail = "ca";
        String newText4Fail = "e";
        String newText5Fail = "tr";
        String newText6Fail = "bg";
        String newText7Fail = "og";

        String phoneNuText ="010s012-3456";

        System.out.println(runDFA.execute(phoneNuText, automatonPhoneNu));

        //System.out.println(runDFA.execute(newText, automaton));
        //System.out.println(runDFA.execute(newText2, automaton2));
        //System.out.println(runDFA.execute(newText3, automaton3));
        //System.out.println(runDFA.execute(newText4, automaton4));
        //System.out.println(runDFA.execute(newText5, automaton5));
        //System.out.println(runDFA.execute(newText6, automaton6));
        //System.out.println(runDFA.execute(newText7, automaton7));

        ////System.out.println(runDFA.execute(newTextFail, automaton));
        //System.out.println(runDFA.execute(newText2Fail, automaton2));
        //System.out.println(runDFA.execute(newText3Fail, automaton3));
        ////System.out.println(runDFA.execute(newText4Fail, automaton4));
        //System.out.println(runDFA.execute(newText5Fail, automaton5));
        //System.out.println(runDFA.execute(newText6Fail, automaton6));
        //System.out.println(runDFA.execute(newText7Fail, automaton7));
    }

    public static void testAlphabet(){
        String newInput = "+(+(z, x), +(g, d))";
        String newInput2 = "°(+(z, x), +(x, z))"; //fail here?
        String newInput3 = "+(°(a, c), °(a, c))";
        String newInput4 = "+(e, e)";
        String newInput5 = "°(+(r, r), t)";
        String newInput6 = "+(+(+(a, b), +(a, b)), +(+(d, x), °(+(j, g), +(a, b))))";
        String newInput7 = "+(+(+(a, b), +(c, z)), +(+(d, x), °(+(j, g), +(r, o))))";
        String newInput8 = "+(+(0, 1), +(1, 0))";
        String newInput9 = "*(°(*(°(0, 1)), *(°(0, 1))))"; //death
        String newInput10 = "*(*(°(0, 1)))";               //works
        String newInput11 = "*(°(*(0), *(1)))";            //works
        String newInput12 = "*(°(*(°(0, 1)), *(°(2, 3))))"; //death
        String newInput13 = "¬(+(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r)))))";
        String newInput14 = "¬(*(°(*(0), *(1))))";
        String newInput15 = "*(°(°(*(°(0, 1)), *(°(2, 3))), °(*(°(4, 5)), *(°(6, 7)))))";
        String newInput16 = "*(°(*(°(°(0, 1), °(2, 3))), *(°(°(4, 5), °(6, 7)))))";
        String newInput17 = "*(°(0, 1))";

        List<String> alphabetNewInput = new ArrayList<String>() {
            { add("z"); add("x"); add("g"); add("d"); }
        };
        List<String> alphabetNewInput2 = new ArrayList<String>() {
            { add("z"); add("x"); }
        };
        List<String> alphabetNewInput3 = new ArrayList<String>() {
            { add("a"); add("c"); }
        };
        List<String> alphabetNewInput4 = new ArrayList<String>() {
            { add("e"); }
        };
        List<String> alphabetNewInput5 = new ArrayList<String>() {
            { add("r"); add("t"); }
        };
        List<String> alphabetNewInput6 = new ArrayList<String>() {
            { add("a"); add("b"); add("d"); add("x"); add("j");add("g"); }
        };
        List<String> alphabetNewInput7 = new ArrayList<String>() {
            { add("a"); add("b"); add("c"); add("z"); add("d");add("x"); add("j");add("g"); add("r");add("o"); }
        };
        List<String> alphabetNewInput8 = new ArrayList<String>() {
            { add("0"); add("1"); }
        };
        List<String> alphabetNewInput9 = new ArrayList<String>() {
            { add("0"); add("1"); }
        };
        List<String> alphabetNewInput10 = new ArrayList<String>() {
            { add("0"); add("1"); }
        };
        List<String> alphabetNewInput11 = new ArrayList<String>() {
            { add("0"); add("1"); }
        };
        List<String> alphabetNewInput12 = new ArrayList<String>() {
            { add("0"); add("1"); add("2"); add("3"); }
        };
        List<String> alphabetNewInput13 = new ArrayList<String>() {
            { add("a"); add("b"); add("c"); add("z"); add("d");add("x"); add("j");add("g"); add("r");add("o"); }
        };
        List<String> alphabetNu = new ArrayList<String>() {
            { add("0"); add("1"); add("2"); add("3"); add("4"); add("5"); add("6"); add("7"); add("8"); add("9"); add("^"); add("-");}
        };
        List<String> alphabetNewInput15 = new ArrayList<String>() {
            { add("0"); add("1"); add("2"); add("3"); add("4"); add("5"); add("6"); add("7"); }
        };
        List<String> alphabetNewInput17 = new ArrayList<String>() {
            { add("0"); add("1"); }

        };


        List<String> gotAlphabet =  getAlphabet(newInput);
        List<String> gotAlphabet2 = getAlphabet(newInput2);
        List<String> gotAlphabet3 = getAlphabet(newInput3);
        List<String> gotAlphabet4 = getAlphabet(newInput4);
        List<String> gotAlphabet5 = getAlphabet(newInput5);
        List<String> gotAlphabet6 = getAlphabet(newInput6);
        List<String> gotAlphabet7 = getAlphabet(newInput7);
        List<String> gotAlphabet8 = getAlphabet(newInput8);
        List<String> gotAlphabet9 = getAlphabet(newInput9);
        List<String> gotAlphabet10 = getAlphabet(newInput10);
        List<String> gotAlphabet11 = getAlphabet(newInput11);
        List<String> gotAlphabet12 = getAlphabet(newInput12);
        List<String> gotAlphabet13 = getAlphabet(newInput13);
        List<String> gotAlphabet14 = getAlphabet(newInput14);
        List<String> gotAlphabet15 = getAlphabet(newInput15);
        List<String> gotAlphabet16 = getAlphabet(newInput16);
        List<String> gotAlphabet17 = getAlphabet(newInput17);

        Collections.sort(gotAlphabet13);
        Collections.sort(alphabetNewInput13);

        System.out.println( gotAlphabet.equals( alphabetNewInput ) );
        System.out.println( gotAlphabet2.equals( alphabetNewInput2 ) );
        System.out.println( gotAlphabet3.equals( alphabetNewInput3 ) );
        System.out.println( gotAlphabet4.equals( alphabetNewInput4 ) );
        System.out.println( gotAlphabet5.equals( alphabetNewInput5 ) );
        System.out.println( gotAlphabet6.equals( alphabetNewInput6 ) );
        System.out.println( gotAlphabet7.equals( alphabetNewInput7 ) );
        System.out.println( gotAlphabet8.equals( alphabetNewInput8 ) );
        System.out.println( gotAlphabet9.equals( alphabetNewInput9 ) );
        System.out.println( gotAlphabet10.equals( alphabetNewInput10 ) );
        System.out.println( gotAlphabet11.equals( alphabetNewInput11 ) );
        System.out.println( gotAlphabet12.equals( alphabetNewInput12 ) );
        System.out.println( gotAlphabet13.equals( alphabetNewInput13 ) );
        System.out.println( gotAlphabet14.equals( alphabetNewInput11 ) );
        System.out.println( gotAlphabet15.equals( alphabetNewInput15 ) );
        System.out.println( gotAlphabet16.equals( alphabetNewInput15 ) );
        System.out.println( gotAlphabet17.equals( alphabetNewInput17 ) );

    }

}