package gitRegexDeriv.src.testing;

import java.util.ArrayList;
import java.util.List;

import gitRegexDeriv.src.DFA.similarityOps.AlphabeticalOrdering;
import gitRegexDeriv.src.main.Atomic;
import gitRegexDeriv.src.main.Atomic.AtomicType;
import gitRegexDeriv.src.main.Commons;

public class findAtomicsUnitTest {

    private List<String> alphabet = new ArrayList<String>() {
        {
            add("a");
            add("b");
            add("c");
            add("d");
            add("g");
            add("j");
            add("o");
            add("r");
            add("x");
            add("z");
        }
    };
    private testDataAtomics testdata = new testDataAtomics();

    public void runAllTestsText() {
        ArrayList<ArrayList<Atomic>> results = new ArrayList<>();
        try {// String inp : testdata.input
            for (int currInput = 0; currInput < testdata.input.size(); currInput++) {
                String inp = testdata.input.get(currInput);
                ArrayList<ArrayList<Atomic>> expResult = testdata.expectedResult.get(currInput);

                System.out.println("Input: " + inp);
                results = AlphabeticalOrdering.findatomicsExtendedAT(inp, String.valueOf(inp.charAt(0)), 0);

                for (int i = 0; i < results.size(); i++) {
                    System.out.println("i : " + i);
                    for (int j = 0; j < results.get(i).size(); j++) {
                        try{
                            System.out.println("Result: " + results.get(i).get(j) + "\n");
                        } catch (IndexOutOfBoundsException e){
                            System.out.println(e.toString());
                        }
                        try{
                            System.out.println("Expectation: " + expResult.get(i).get(j) + "\n");
                        } catch (IndexOutOfBoundsException e){
                            System.out.println(e.toString());
                        }
                    }
                }
                System.out.println();
            }
        } catch (StackOverflowError t) {
            System.out.println("Caught " + t);
            // t.printStackTrace();
        }
    }

    public void runAllTests() {

        ArrayList<ArrayList<Atomic>> results = new ArrayList<>();
        try {
            for (int currInput = 0; currInput < testdata.input.size(); currInput++) {
                String inp = testdata.input.get(currInput);
                String mainOp = Commons.ops.contains(String.valueOf(inp.charAt(0))) ? String.valueOf(inp.charAt(0))
                        : "";
                ArrayList<ArrayList<Atomic>> expResult = testdata.expectedResult.get(currInput);

                System.out.println("Input: " + inp);
                results = AlphabeticalOrdering.findatomicsExtendedAT(inp, mainOp, 0);
                for (int i = 0; i < results.size(); i++) {
                    for (int j = 0; j < results.get(i).size(); j++) {
                        assert (results.get(i).get(j).equals(expResult.get(i).get(j)));
                    }
                }
            }
        } catch (StackOverflowError t) {
            System.out.println("Caught " + t);
        }
    }

    public void runAllTestsSort() {
        ArrayList<Atomic> results = new ArrayList<>();

        try {
            for (int currInput = 0; currInput < testdata.inputSorting.size(); currInput++) {
                String inp = testdata.inputSorting.get(currInput);
                String mainOp = Commons.ops.contains(String.valueOf(inp.charAt(0))) ? String.valueOf(inp.charAt(0))
                        : "";
                ArrayList<ArrayList<Atomic>> expResult = testdata.expectedSortedResult.get(currInput);

                System.out.println("Input: " + inp);
                results = AlphabeticalOrdering.sortRegex( AlphabeticalOrdering.findatomicsExtendedAT(inp, mainOp, 0).get(0), inp );
                for (int i = 0; i < results.size(); i++) {
                    System.out.println(results.get(i));
                }
                System.out.println("\n");
            }
        } catch (StackOverflowError t) {
            System.out.println("Caught " + t);
        }
    }

    public void runOneSortTest(int index) {
        try {
            String inp = testdata.inputSorting.get(index);
            String mainOp = Commons.ops.contains(String.valueOf(inp.charAt(0))) ? String.valueOf(inp.charAt(0))
                            : "";

            ArrayList<ArrayList<Atomic>> expResult = testdata.expectedSortedResult.get(index);

            var result = AlphabeticalOrdering.sortRegex( AlphabeticalOrdering.findatomicsExtendedAT(inp, mainOp, 0).get(0), inp);

            System.out.println(result);


        } catch (Exception e){
            System.out.println(e.toString());
        }
    }

    public void runOneTest(int index) {
        try {

            String inp = testdata.input.get(index);
            ArrayList<ArrayList<Atomic>> expResult = testdata.expectedResult.get(index);
            ArrayList<ArrayList<Atomic>> result = AlphabeticalOrdering.findatomicsExtendedAT(inp,
                    String.valueOf(inp.charAt(0)), 0);

            for (int i = 0; i < expResult.size(); i++) {
                for (int j = 0; j < expResult.get(i).size(); j++) {
                    assert (result.get(i).get(j).equals(expResult.get(i).get(j)));
                }
            }
        } catch (StackOverflowError t) {

            System.out.println("Caught " + t);
        }
    }

    public void runOneTest(String input, ArrayList<ArrayList<Atomic>> expectedRes) {
        try {
            String inp = input;
            ArrayList<ArrayList<Atomic>> expResult = expectedRes;

            ArrayList<ArrayList<Atomic>> result = AlphabeticalOrdering.findatomicsExtendedAT(inp,
                    String.valueOf(inp.charAt(0)), 0);

            for (int i = 0; i < result.size(); i++) {

                for (int j = 0; j < result.get(i).size(); j++) {
                    System.out.println("Result: " + result.get(i).get(j) + "\n");
                    System.out.println("Expectation: " + expResult.get(i).get(j) + "\n");
                    assert (result.get(i).get(j).equals(expResult.get(i).get(j)));
                    System.out.println("pass\n");
                }
            }
            System.out.println();

        } catch (StackOverflowError t) {
            System.out.println("Caught " + t);
        }
    }

    public void runClassOfTest(String classOfTestName) {

    }

    class testDataAtomics {
        private List<String> input = new ArrayList<String>() {
            {
                add("+(°(a, b), °(a, c))"); // [ [2, 11] ]
                add("°(a, b)"); // [ [0] ]
                add("+(a, b)"); // [ [2, 5] ]
                add("°(°(a, b), °(a, c))"); // [ [0], [2], [11] ]
                add("*(a)"); // [ [0] ]
                add("*(+(a, *(b)))"); // [ [0], [4, 7] ]
                add("*(°(*(a), b))"); // [ [0], [2], [4] ]
                add("+(+(a, b), °(°(a, c), °(a, a)))"); // [ [4, 7, 11], [13, 22] ]
                add("a"); // [ [0] ]
                add("+(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r))))"); // [ [] ] mhm
                add("°(+(a, d), +(c, b))"); // [ [0], [4, 7], [13, 16] ]
                add("°(+(a, b), c)");// [ [0], [4, 7] ]
                // input[10] = "-(a)"; //OUT OF BOUNDS EXCEPTION TODO IMPLEMENT NEGATION
            }
        };

        private List<String> inputSorting = new ArrayList<String>() {
            {
                add("+(°(a, b), °(a, c))"); // [ [2, 11] ] sorted by default, marked with - 0
                add("+(°(a, c), °(a, b))"); // [ [2, 11] ] unsorted variant
                add("°(a, b)"); // [ [0] ] - 1
                add("°(b, a)"); // [ [0] ]
                add("+(a, b)"); // [ [2, 5] ] - 2
                add("+(b, a)"); // [ [2, 5] ]
                add("°(°(a, b), °(a, c))"); // [ [0], [2], [11] ] - 3
                add("°(°(a, c), °(a, b))"); // [ [0], [2], [11] ]
                add("*(a)"); // [ [0] ] - 4
                add("*(+(a, *(b)))"); // [ [0], [4, 7] ] - 5
                add("*(+(b, *(a)))"); // [ [0], [4, 7] ]
                add("*(°(*(a), b))"); // [ [0], [2], [4] ] - 6
                add("*(°(*(b), a))"); // [ [0], [2], [4] ]
                add("+(+(a, b), °(°(a, c), °(a, a)))"); // [ [4, 7, 11], [13, 22] ] - 7
                add("+(+(a, c), °(°(a, b), °(a, a)))"); // [ [4, 7, 11], [13, 22] ]
                add("a"); // [ [0] ] - 8
                add("+(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r))))"); // [ [] ] mhm (well said, past me) - 9
                add("+(+(+(d, x), +(g, j)), +(+(a, b), °(+(c, z), +(o, r))))");
                add("°(+(a, d), +(c, b))"); // [ [0], [4, 7], [13, 16] ] - 10
                add("°(+(c, b), +(a, d))"); // [ [0], [4, 7], [13, 16] ]
                add("°(+(a, b), c)");// [ [0], [4, 7] ] - 11
                add("°(c, +(a, b))");// [ [0], [4, 7] ]
                // input[10] = "-(a)"; //OUT OF BOUNDS EXCEPTION TODO IMPLEMENT NEGATION
            }
        };

        private List<ArrayList<ArrayList<Atomic>>> expectedSortedResult = new ArrayList<>() {
            {
                // 0 -
                // +(°(a, b), °(a, c))"
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() { // depth 0
                            { // data
                                add(new Atomic(AtomicType.SUBREGEX, 2, 7, "+"));
                                add(new Atomic(AtomicType.SUBREGEX, 11, 7, "+"));
                            }
                        });
                    }
                });
                // 0
                // +(°(a, c), °(a, b))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() { // depth 0
                            { // data
                                add(new Atomic(AtomicType.SUBREGEX, 11, 7, "+"));
                                add(new Atomic(AtomicType.SUBREGEX, 2, 7, "+"));
                            }
                        });
                    }
                });

                // 1 -
                // °(a, b)
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 7, "°"));
                            }
                        });
                    }
                });
                // 1
                // °(b, a)
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 7, "°"));
                            }
                        });
                    }
                });

                // 2 -
                // +(a, b)
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 2, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 5, 1, "+"));
                            }
                        });
                    }
                });
                // 2
                // +(b, a)
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 5, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 2, 1, "+"));
                            }
                        });
                    }
                });

                // 3 -
                // °(°(a, b), °(a, c))
                add(new ArrayList<>() {
                    {// depth 0
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 19, "°"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 2, 7, "°"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 11, 7, "°"));
                            }
                        });
                    }
                });
                // 3
                // °(°(a, c), °(a, b))
                add(new ArrayList<>() {
                    {// depth 0
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 19, "°"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 2, 7, "°"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 11, 7, "°"));
                            }
                        });
                    }
                });

                // 4
                // *(a)
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 4, "*"));
                            }
                        });
                    }
                });


                // 5 *(+(a, *(b))) -
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 13, "*"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                                add(new Atomic(AtomicType.SUBREGEX, 7, 4, "+"));
                            }
                        });
                    }
                });
                // 5 *(+(b, *(a)))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 13, "*"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 7, 4, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));

                            }
                        });
                    }
                });

                // 6 -
                // *(°(*(a), b))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 13, "*"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 2, 10, "*"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 4, 4, "°"));
                            }
                        });

                    }
                });
                // 6
                // *(°(*(b), a))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 13, "*"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 2, 10, "*"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 4, 4, "°"));
                            }
                        });

                    }
                });

                // 7 -
                // +(+(a, b), °(°(a, c), °(a, a)))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                                add(new Atomic(AtomicType.SUBREGEX, 11, 19, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 13, 7, "°"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 22, 7, "°"));
                            }
                        });
                    }
                });
                // 7
                // +(+(a, c), °(°(a, b), °(a, a)))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 11, 19, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 13, 7, "°"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 22, 7, "°"));
                            }
                        });
                    }
                });

                // 8 -
                // a
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 0, 1, ""));
                            }
                        });
                    }
                });

                // 9 +(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r)))) -

                //+(+(+(a, b), +(c, d)), +(°(+(g, j), +(o, r)), +(x, z)))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 6, 1, "+"));//a
                                add(new Atomic(AtomicType.CHARACTER, 9, 1, "+"));//b
                                add(new Atomic(AtomicType.CHARACTER, 15, 1, "+"));//c
                                add(new Atomic(AtomicType.CHARACTER, 27, 1, "+"));//d
                                add(new Atomic(AtomicType.SUBREGEX, 34, 19, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 30, 1, "+"));//x
                                add(new Atomic(AtomicType.CHARACTER, 18, 1, "+"));//z
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 38, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 41, 1, "+"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 47, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 50, 1, "+"));
                            }
                        });
                    }
                });
                // 9 +(+(+(d, x), +(g, j)), +(+(a, b), °(+(c, z), +(o, r))))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 27, 1, "+"));//a
                                add(new Atomic(AtomicType.CHARACTER, 30, 1, "+"));//b
                                add(new Atomic(AtomicType.SUBREGEX, 34, 19, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 6, 1, "+"));//d
                                add(new Atomic(AtomicType.CHARACTER, 15, 1, "+"));//g
                                add(new Atomic(AtomicType.CHARACTER, 18, 1, "+"));//j
                                add(new Atomic(AtomicType.CHARACTER, 9, 1, "+"));//x
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 38, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 41, 1, "+"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 47, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 50, 1, "+"));
                            }
                        });
                    }
                });

                // 10 °(+(a, d), +(c, b)) -
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 19, "°"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 13, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 16, 1, "+"));
                            }
                        });
                    }
                });
                // 10 °(+(c, b), +(a, d))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 19, "°"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 13, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 16, 1, "+"));
                            }
                        });
                    }
                });

                // 11 °(+(a, b), c) -
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 13, "°"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                            }
                        });
                    }
                });
                // 11 °(c, +(a, b))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 13, "°"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 10, 1, "+"));
                            }
                        });
                    }
                });
            }
        };


        private List<ArrayList<ArrayList<Atomic>>> expectedResult = new ArrayList<>() {
            {
                // 0
                // +(°(a, b), °(a, c))"
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() { // depth 0
                            { // data
                                add(new Atomic(AtomicType.SUBREGEX, 2, 7, "+"));
                                add(new Atomic(AtomicType.SUBREGEX, 11, 7, "+"));
                            }
                        });

                    }
                });

                // 1
                // °(a, b)
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 7, "°"));
                            }
                        });
                    }
                });

                // 2
                // +(a, b)
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 2, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 5, 1, "+"));
                            }
                        });
                    }
                });

                // 3
                // °(°(a, b), °(a, c))
                add(new ArrayList<>() {
                    {// depth 0
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 19, "°"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 2, 7, "°"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 11, 7, "°"));
                            }
                        });
                    }
                });

                // 4
                // *(a)
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 4, "*"));
                            }
                        });
                    }
                });

                // 5 *(+(a, *(b)))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 13, "*"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                                add(new Atomic(AtomicType.SUBREGEX, 7, 4, "+"));
                            }
                        });
                    }
                });

                // 6
                // *(°(*(a), b))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 13, "*"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 2, 10, "*"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 4, 4, "°"));
                            }
                        });

                    }
                });

                // 7
                // +(+(a, b), °(°(a, c), °(a, a)))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                                add(new Atomic(AtomicType.SUBREGEX, 11, 19, "+"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 13, 7, "°"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 22, 7, "°"));
                            }
                        });
                    }
                });

                // 8
                // a
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 0, 1, ""));
                            }
                        });
                    }
                });

                // 9 +(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r))))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 6, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 9, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 15, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 18, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 27, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 30, 1, "+"));
                                add(new Atomic(AtomicType.SUBREGEX, 34, 19, "+"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 38, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 41, 1, "+"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 47, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 50, 1, "+"));
                            }
                        });
                    }
                });

                // 10 °(+(a, d), +(c, b))
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 19, "°"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                            }
                        });
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 13, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 16, 1, "+"));
                            }
                        });
                    }
                });

                // 11 °(+(a, b), c)
                add(new ArrayList<>() {
                    {
                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.SUBREGEX, 0, 13, "°"));
                            }
                        });

                        add(new ArrayList<>() {
                            {
                                add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                                add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                            }
                        });
                    }
                });
            }
        };

    }

    /* These tests are probably wrong, and shouldn't be used anymore.
        They are here just in case

    private List<ArrayList<ArrayList<Atomic>>> expectedResult_old = new ArrayList<>() {
        {
            // 0
            add(new ArrayList<>() {
                { // depth 0
                    add(new ArrayList<>() {
                        { // data
                            add(new Atomic(AtomicType.SUBREGEX, 2, 7, "+"));
                            add(new Atomic(AtomicType.SUBREGEX, 11, 7, "+"));
                        }
                    });
                }
            });

            // 1
            add(new ArrayList<>() {
                {
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 0, 7, "°"));
                        }
                    });
                }
            });

            // 2
            add(new ArrayList<>() {
                {
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.CHARACTER, 2, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 5, 1, "+"));
                        }
                    });
                }
            });

            // 3
            add(new ArrayList<>() {
                {// depth 0
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 0, 19, "°"));
                        }
                    });
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 2, 7, "°"));
                            add(new Atomic(AtomicType.SUBREGEX, 11, 7, "°"));
                        }
                    });
                }
            });

            // 4
            add(new ArrayList<>() {
                {
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 0, 4, "*"));
                        }
                    });
                }
            });

            // 5 *(+(a, *(b)))
            add(new ArrayList<>() {
                {
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 0, 13, "*"));
                        }
                    });

                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                            add(new Atomic(AtomicType.SUBREGEX, 7, 4, "+"));
                        }
                    });
                }
            });

            // 6 *(°(*(a), b))
            add(new ArrayList<>() {
                {
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 0, 13, "*"));
                        }
                    });

                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 2, 10, "*"));
                        }
                    });

                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 4, 4, "°"));
                        }
                    });

                }
            });

            // 7 +(+(a, b), °(°(a, c), °(a, a)))
            add(new ArrayList<>() {
                {
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                            add(new Atomic(AtomicType.SUBREGEX, 11, 19, "+"));
                        }
                    });

                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 13, 7, "°"));
                            add(new Atomic(AtomicType.SUBREGEX, 22, 7, "°"));
                        }
                    });
                }
            });

            // 8 a
            add(new ArrayList<>() {
                {
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.CHARACTER, 0, 1, ""));
                        }
                    });
                }
            });

            // 9 +(+(+(a, b), +(c, z)), +(+(d, x), °(+(g, j), +(o, r))))
            add(new ArrayList<>() {
                {
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.CHARACTER, 6, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 9, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 15, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 18, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 27, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 30, 1, "+"));
                            add(new Atomic(AtomicType.SUBREGEX, 34, 19, "+"));
                        }
                    });

                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.CHARACTER, 38, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 41, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 47, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 50, 1, "+"));
                        }
                    });
                }
            });

            // 10 °(+(a, d), +(c, b))
            add(new ArrayList<>() {
                {
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 0, 19, "°"));
                        }
                    });

                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 13, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 16, 1, "+"));
                        }
                    });
                }
            });

            // 11 °(+(a, b), c)
            add(new ArrayList<>() {
                {
                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.SUBREGEX, 0, 13, "°"));
                        }
                    });

                    add(new ArrayList<>() {
                        {
                            add(new Atomic(AtomicType.CHARACTER, 4, 1, "+"));
                            add(new Atomic(AtomicType.CHARACTER, 7, 1, "+"));
                        }
                    });
                }
            });
        }
    };
*/
}
